<?php

return [

	'modules'  => [
		[ 'key' => 'app', 'label' => 'App'   ],
		[ 'key' => 'cnf', 'label' => 'Config'],
		[ 'key' => 'usr', 'label' => 'User'  ], 	
 	],

 	'values' => [
 		[ 'type' => 'checkbox', 'key' => 'create_config',      'value' => '1' ],
		[ 'type' => 'checkbox', 'key' => 'create_controllers', 'value' => '1' ],
		[ 'type' => 'checkbox', 'key' => 'create_requests',    'value' => '1' ],
		[ 'type' => 'checkbox', 'key' => 'create_migrations',  'value' => '1' ],
		[ 'type' => 'checkbox', 'key' => 'create_models',      'value' => '1' ],
		[ 'type' => 'checkbox', 'key' => 'create_paths',       'value' => '1' ],
		[ 'type' => 'checkbox', 'key' => 'create_views',       'value' => '1' ],
	]

];