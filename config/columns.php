<?php

	return [	
		'groups' => [
			"constant"  => 'Constant',
			"date"		=> 'Date',
			"relation"  => 'Relation',
			"config"    => 'Config',
			"enum"      => 'Enum',
			"dynum"     => 'Dynum',
		],



		'ids' => [
			'inc'        => 'Increments',
			'primary'    => 'Primary',
			'unique'     => 'Unique',
			'index'      => 'Index'
		],

		'constants' => [
			'text'      => 'Text',
			'char'      => 'Char',
			'integer'   => 'Integer',
			'boolean'   => 'Boolean',
			'float'     => 'Float',
		],

		'dates'		=> [
			'date-time' => 'Date Time',
			'time'      => 'Time',
			'date'      => 'Date',
		],

		'relations'	=> [
			'parent-id'    => 'Of Parent',
			'one-to-one'   => 'One to One',
			'one-to-many'  => 'One of Many',
			'many-to-many' => 'Many of Many',
		],
	];