<?php 

	$yeti_folder     = '/Projects/yeti/';
	$public_path     = 'public/';
	$assets_path     = 'resources/assets/';
	$bootstrap_sass  = 'brickle.scss';

	echo "\nHello Foundation\n\n";

	$cwd= explode( DIRECTORY_SEPARATOR, getcwd());
	$last = array_pop($cwd);
	if ( $last == 'movers' ){
		$public_path     = "../".$public_path;
		$assets_path     = "../".$assets_path;
	}

	if ( !file_exists($yeti_folder.'css/foundation.min.css') )
	{
		echo "expect '{$yeti_folder}css/foundation.min.css' not found.\n";
		exit;
	}
	if ( !file_exists($yeti_folder.'bower_components/foundation-sites/dist/foundation.min.js') )
	{
		echo "expect '{$yeti_folder}bower_components/foundation-sites/dist/foundation.min.js' not found.\n";
		exit;
	}

	$js= file_get_contents($yeti_folder.'css/foundation.min.css');
	file_put_contents($public_path.'css/foundation.min.css', $js);	

	$js= file_get_contents($yeti_folder.'bower_components/foundation-sites/dist/foundation.min.js');
	file_put_contents($public_path.'js/foundation.min.js', $js);	

	echo "Done.\n\n";

	exit;


function osx_path($path)
{
	$path = str_replace([' ','(',')'], ['\ ','\(','\)'], $path);
	return $path;
}


