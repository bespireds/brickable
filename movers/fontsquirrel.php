<?php 

	$download_folder = '/Users/joeri/Downloads/';
	$public_path     = "public/";
	$assets_path     = "resources/assets/";
	$bootstrap_sass  = "brickle.scss";

	echo "\nHello Fontsquirrel\n\n";

	$cwd= explode( DIRECTORY_SEPARATOR, getcwd());
	$last = array_pop($cwd);
	if ( $last == 'movers' ){
		$public_path     = "../".$public_path;
		$assets_path     = "../".$assets_path;
	}
	
	$otime = 0;
	$ffile = '';
	foreach (scandir($download_folder) as $entry)
	{
		if ( substr($entry, -4) == '.zip'){
			if ( substr($entry, 0, 10) == 'webfontkit'){
				$ftime = filemtime( $download_folder . $entry );
				if ( $ftime > $otime ){
					$otime= $ftime;
					$ffile= $entry;
				}
			}
		}
	}

	if( $ffile == '' ){
		echo "No webfontkit.zip found in $download_folder\n";
		exit;
	}

	echo "$ffile found in $download_folder\n";

	// unzip the webfontkit.zip
	if ( file_exists($download_folder.'fontsquirrel')) {
		$cmd= "rm -r ".osx_path($download_folder.'fontsquirrel/*');
		shell_exec($cmd);		
	}
	if (!file_exists($download_folder.'fontsquirrel')) mkdir($download_folder.'fontsquirrel');
	
	$cmd= "cd ".osx_path($download_folder).";cp ".osx_path($ffile).' '.osx_path($download_folder.'fontsquirrel');
	$cmd.= ";cd ./fontsquirrel; unzip ".osx_path($ffile);
	shell_exec($cmd);

	//figure out the name of the font
	$font_name = '';
	$font_folder = $download_folder.'fontsquirrel/';
	foreach (scandir($font_folder) as $entry)
	{
		if ( substr($entry, -4) == '.ttf') $font_name = substr( $entry, 0, -4 );
	}

	if( $font_name == '' ){
		echo "No ttf found in $font_folder\n";
		exit;
	}
	$full_font_name= $font_name;
	$dash = strpos($font_name, '-');
	if ( $dash > 0 ) $font_name = substr($full_font_name, 0, $dash);

	// copy font to public folder
	$font_public_path = "{$public_path}font/$font_name/";
	if (!file_exists($font_public_path)) mkdir($font_public_path);
	$cmd= "cp ".osx_path("{$font_folder}{$font_name}*").' '.osx_path("$font_public_path");
	echo $cmd."\n";
	shell_exec($cmd);

	
	// copy font to asset folder
	$resource_assets_path = "{$assets_path}font";
	if (!file_exists($resource_assets_path)) mkdir($resource_assets_path);
	
	$resource_assets_path = "{$assets_path}font/$font_name/";
	if (!file_exists($resource_assets_path)) mkdir($resource_assets_path);
	// $zipfile = preg_replace($re, '', $ffile, 1);
	
	$cmd= "cp ".osx_path("{$download_folder}{$ffile}").' '.osx_path("{$resource_assets_path}{$ffile}");
	echo $cmd."\n";
	shell_exec($cmd);

	// change scss to sass
	$css_folder = $download_folder . $font_folder_name . '/fontsquirrel/';
	$css_file = $css_folder . 'stylesheet.css';
	$css = file_get_contents($css_file);
	$scss= str_replace("url('$font_name", "url('/font/$font_name/$font_name", $css);
	file_put_contents("{$assets_path}sass/vendor/$font_name.scss", $scss);
	
	// remove the unzipped folder
	$cmd= "rm -R ".osx_path($download_folder.'fontsquirrel');
	echo $cmd."\n";
	shell_exec($cmd);

	echo "\n";
	if ($bootstrap_sass != ''){
		$bootstrap = file_get_contents("{$assets_path}sass/{$bootstrap_sass}");
		if ( strpos($bootstrap, "vendor/$font_name") === false )
		{
			$bootstrap	= "@import \"vendor/$font_name\";\n" . $bootstrap;
			file_put_contents("{$assets_path}sass/{$bootstrap_sass}", $bootstrap);
		}
	}else{
		echo "Please add @import \"vendor/$font_name\"; to your bootstrap sass.\n";
	}

	echo "Done.\n\n";

	exit;


function osx_path($path)
{
	$path = str_replace([' ','(',')'], ['\ ','\(','\)'], $path);
	return $path;
}


