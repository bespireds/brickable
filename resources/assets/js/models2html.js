
function relationize(relation_id)
{
	var relation= $('[data-id="#"]'.replace('#', relation_id)).attr('data-name');
	return relation;
}

function nicename(relation_id, type)
{
	var relation= $('[data-id="#"]'.replace('#', relation_id)).attr('data-name');

	if ( typeof relation == 'undefined' ) return '';
	
	if ((relation.substr(-1) == 's') && (type == 'one-to-one'))
	{
		relation= relation.substr(0,relation.length-1);
	}
	relation = relation.toLowerCase().replace(/\b[a-z]/g, function(letter) {
    	return letter.toUpperCase();
	});

	relation = relation.replace(/\-/g, "&#8209;")

	return relation;
}

function modelreplace(id)
{
	var url = '/model/async/#/show'.replace('#', id);
	var model_id = id;
	var exists = $('#models [data-id="{id}"]'.replace('{id}',id)).length;

	var jqxhr = $.ajax( url )
		.done(function(data){
		
			if (!exists) 
			{
				model2html(data.model);
				drag_binding();
			}

			var header = getStub('update-header')
				.replace('[id]', model_id)
				.replace('[name]', data['model'].name);

			$('[data-id="#"] .header'.replace('#', model_id)).html(header);
			$('[data-id="#"] .body'.replace('#', model_id)).html('');

			var columns = data['columns'];
			for (var i = columns.length - 1; i >= 0; i--) {
				column2html(columns[i]);
			}

			connectboxes();
			wired();
			$('#editModal').trigger('modal:hide', $('#editModal')).html('');
		});
}

function modelupdate(id, action, var1, var2)
{
	var jqxhr = $.ajax( "/model/async/"+id+"/"+action+"/"+var1+"/"+var2 )
		.done(function(data){});
}

function combi2html(data)
{

	var models = data['models'];
	for (var i = models.length - 1; i >= 0; i--) {
		model2html(models[i]);
	}
	var columns = data['columns'];
	for (var i = columns.length - 1; i >= 0; i--) {
		column2html(columns[i]);
	}

}

function column2html(column){

//	console.log( column );

	var field = getStub('html-field');

	var dataconnect = '',
		relation = column['type'];
	
	if (column['group'] == 'relation'){
		var relation_id = column['relation_id'];
		if ( relation_id ){

			if (column['type'] != 'parent-id')
			{
				dataconnect= 'data-connect="'+relationize(relation_id)+'"';
			}
			relation= nicename(relation_id, column['type']);
		}
	}
	
	if (column['title'].length) relation = column['title'];
	var is_narrow = '';
	if ( relation.length > 14 ) is_narrow = 'narrow';

	field = field.replace(/\[typecol\]/g, column['type']);
	field = field.replace(/\[connect\]/g, dataconnect);
	field = field.replace(/\[reltype\]/g, relation);
	field = field.replace(/\[name\]/g,    column['name']);
	field = field.replace(/\[narrow\]/g,  is_narrow);

	var selector = "[data-id='#'] .body".replace('#', column['model_id'] );
	
	$(selector).prepend(field);

}

function model2html(model){
	
	var modelname = model.name.toLowerCase().replace('(s)', 's');

	var html = getStub('html-model');

	html = html.replace(/\[zidx\]/g,   model.id );
	html = html.replace(/\[type\]/g,   model.type );
	html = html.replace(/\[mname\]/g,  modelname );
	html = html.replace(/\[id\]/g,     model.id );
	html = html.replace(/\[x\]/g,      model.screen_left );
	html = html.replace(/\[y\]/g,      model.screen_top );
	html = html.replace(/\[name\]/g,   model.name );

	console.log( model );

	$( "#models" ).append( html );
//	console.log( model );
}

function connectboxes()
{
//	var stub='<div id="[id]"><div class="box-in"></div><div class="box-out"></div></div>';	
	var stub=''
	stub += '<div id="[id]">';	
	stub += '<svg width="190px" height="160px" version="1.1" xmlns="http://www.w3.org/2000/svg">';
  	stub += '<path d="M10 80 Q 52.5 10, 95 80 T 180 80" stroke="#222" stroke-width=".5" fill="transparent"/>';	
//  	stub += '<circle cx="5" cy="5" r="4" stroke="black" stroke-width="" fill="red"></circle>';
	stub += '</svg>';
	stub += '</div>';

	$("#wires").html('');
	$('[data-connect]').each(function(idx){ 

		var mynameis  = $(this).attr('data-connect');
		var modelname = '[data-name="#"]'.replace('#',mynameis);
		if ($(modelname).length > 0 )
		{
//			var id=  $(this).parent().parent().attr('data-name') + '-' + mynameis;
			var id = 'wire-' + idx;

			if ($('#'+id).length == 0 )
			{
				$("#wires").append(stub.replace('[id]', id));
				$(this).attr('data-wire', id)
			}
		}
	});
}
