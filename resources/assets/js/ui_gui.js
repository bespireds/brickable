
var pointerX;
var pointerY;
var zoom = 1;

$('#wires, #models').css({zoom: zoom});

function drag_binding(gridsize)
{
	var zoomgridsize = gridsize / zoom;

	$( '.model' ).draggable({
		start: function( event, ui ) {
			pointerY = (event.pageY - $('#models').offset().top) / zoom - parseInt($(event.target).css('top'));
			pointerX = (event.pageX - $('#models').offset().left) / zoom - parseInt($(event.target).css('left'));
		},
		drag: function( event, ui ) {
			var canvasTop= 0;
			var canvasLeft= 0;
			
			if (gridsize){
				event.pageY = Math.round( event.pageY / zoomgridsize ) * zoomgridsize;
				event.pageX = Math.round( event.pageX / zoomgridsize ) * zoomgridsize;
				pointerY =  Math.round( pointerY / zoomgridsize ) * zoomgridsize;
				pointerX =  Math.round( pointerX / zoomgridsize ) * zoomgridsize;
					
			}

			ui.position.top  = Math.round((event.pageY - canvasTop) / zoom - pointerY); 
			ui.position.left = Math.round((event.pageX - canvasLeft) / zoom - pointerX); 

			ui.offset.top = Math.round(ui.position.top + canvasTop);
			ui.offset.left = Math.round(ui.position.left + canvasLeft);

			wired();
		},
		stop: function( event, ui ) {
			var id= $(ui.helper[0]).attr('data-id');
			var rndL = Math.floor(ui.position.left * zoom);
			var rndT = Math.floor(ui.position.top * zoom);
			modelupdate( id, 'position', rndL, rndT );
			wired();
		}
	});
}


$(function() {

	var project_id= $('#models').attr('data-project-id');

// Edit window

	if ( project_id > 0 )
	{
		var jqxhr = $.ajax( "/project/async/" + project_id)
			.done(function(data) {

			combi2html(data);
			connectboxes();
			wired();

			drag_binding( null );
			
			$( '.model' ).on('mousedown', function(event) { 
				$('.model').css('z-index','1');
				$( this ).css('z-index','1000');
			});
		});
	}

// Modals

	$(document).on('modal:show', function(e,d){
		$(d).addClass('open');
		$(document).find('.reveal-modal-bg').addClass('open');
	})
	$(document).on('modal:hide', function(e,d){
		$(document).find('.reveal-modal-bg').removeClass('open');
		$(d).removeClass('open');
	})

	$(document).on('click', '[data-close-modal]', function(){ 
		$(document).trigger('modal:hide', $(this).closest('[data-reveal]'));
	})

	$(document).on('click', '[data-sure]', function(e){
		if ( $(this).next().hasClass('open') )
			$(this).next().removeClass('open');
		else
			$(this).next().addClass('open');
	});

	$(document).on('click', '[data-not-sure]', function(e){
		$(this).closest('.sure.open').removeClass('open');
	});


// Legenda
	
	$('[data-action="zoom-in"]').click( function(e){
		zoom -= 0.1;
		e.preventDefault();
		$('#wires, #models').css({zoom: zoom});
	});
	$('[data-action="zoom-out"]').click( function(e){
		zoom += 0.1;
		e.preventDefault();
		$('#wires, #models').css({zoom: zoom});
	});


	$('[data-action="grid"]').click( function(e){
		if ( $(this).hasClass('active')){
			$(this).removeClass('active');
			drag_binding( null );
		}else{
			$(this).addClass('active');
			drag_binding( 20 );
			// and put all on grid...
		}
	});


// Modals

	$(document).on('click', '[data-close-modal]', function(e){
		$(this).parent().removeClass('open');
	});

// Any row in a Modal

	$(document).on('click', '.delete-box', function(e){
		$(this).closest('li').remove();
	});

});