
$(function() {

	var project_id= $('#models').attr('data-project-id');

// Sidebar
	$('[data-action="add-model"]').click( function(e){
	
		$('#editModal').html(getStub('modal-model')).trigger('modal:show', '#editModal');
		$.ajax("/model/async/{id}/create".replace('{id}', $('[data-project-id]').attr('data-project-id') ))
			.done(function(response){ 
				$('#editModal').html(response); 
			});

	})

// Modals

	$(document).on('click', '[data-model-edit]', function(e){
		e.preventDefault();
		$('#editModal').html(getStub('modal-model')).trigger('modal:show', '#editModal');
		$.ajax("/model/async/{id}/edit/".replace('{id}', $(this).attr('data-model-edit') ))
			.done(function(response){ $('#editModal').html(response); });
	});

	$(document).on('change', '[name="column_group[]"]', function(e){
		var $li = $(this).parent().parent();
		$li.attr('class', $(this).val());
	});

	$(document).on('click', '[data-model-delete]', function(e){
		var id = $(this).attr('data-model-delete');
		var url= "/model/async/{id}/delete".replace('{id}', id);

		$.ajax(url).done(function(response){ 
			$('[data-id="#"]'.replace('#',id)).remove();
			$('#wires').html('');
			connectboxes();
			wired();

			$('#editModal').trigger('modal:hide', '#editModal').html('');
		});

		

	});

	$(document).on('click', '[data-add-modelrow]', function(e){
		$('#editModal .columns').append($('.blueprint').html());
		$('#editModal .columns').sortable();
		$('#editModal ul').disableSelection();
	});

	$(document).on('click', '[data-save-model]', function(e){
		
		var form     = $('#editModal').find('form');
		var model_id = form.attr('data-model-id');
		if ( model_id == '' ) model_id = 0;
		var url      = "/model/async/{id}/update".replace('{id}', model_id);

		$('#editModal ul').hide();
		$('#editModal .header input').hide();
		$('#editModal .header').append('Saving...');
		
		$.ajax( { url:url, type: "POST", data: form.serialize() })
			.done(function(response){ 
					
				modelreplace(response.id); 

			});
	
	});


});