
$(function() {

	
	var project_id= $('#models').attr('data-project-id');

// Sidebar
//	$('[data-action="project"]').click( function(e){
//		$(document).trigger('modal:show', $('#projectModal'));
//	});
// Sidebar
	$('[data-action="project"]').click( function(e){
	
		$('#projectModal').html(getStub('modal-project')).trigger('modal:show', '#projectModal');
		$.ajax("/project/async/{id}/edit".replace('{id}', $('[data-project-id]').attr('data-project-id') ))
			.done(function(response){ 
				$('#projectModal').html(response); 
			});

	})

	$('[data-upload]').click( function(e){
		
		var upload = $(this).attr('data-upload');
		var fileSelect = document.getElementById('file-select');
		var files = fileSelect.files;
		var formData = new FormData();
		for (var i = 0; i < files.length; i++) {
  			var file = files[i];
  			formData.append('files[]', file, file.name);
		}
		formData.append('uploadtype', upload);
		formData.append('project_id', $('#models').attr('data-project-id') );
		
		var url = '/project/async/import';
		var xhr = new XMLHttpRequest();
		xhr.open('POST', url, true);
		xhr.onload = function () {
			if (xhr.status === 200) {
				var response = JSON.parse(this.responseText);
				window.location = '/project/' + response;
			} 
		};
		xhr.send(formData);

	})

// Upload
// Sidebar
	$('#file-select').change(function(){
		var fileSelect = document.getElementById('file-select');
		var files = fileSelect.files;
		var formData = new FormData();
		for (var i = 0; i < files.length; i++) {
  			var file = files[i];
  			formData.append('files[]', file, file.name);
		}
		$(document).trigger('modal:show', $('#uploadModal'));

	});

	// make li clickable
	$('.projects li:not(.new)').click( function(e){
		var $li= $(this);
		setTimeout( function(){
			var locked = $li.hasClass('locked');
			if ( !locked )
			{
				var href= $li.find('a').attr('href');
				window.location.href= href;
			}
		}, 10);
	});


// Modals

	$(document).on('click', '[data-save-project]', function(e){
		var name = $('[name="projectname"]').val();
		var post = "/project/async/rename/" + project_id + '/' + encodeURIComponent(name);
		var url  = "/project/async/{id}/update".replace('{id}', project_id);
		var form = $('#projectModal').find('form');
		
		$.ajax( { url:url, type: "POST", data: form.serialize() })
			.done(function(response){ 
					
				$.ajax(post).done(	function(response){ 
					name = response.name;
					document.title = name;
					$('[name="projectname"]').val(name);
					$('[data-project-name]').attr('data-project-name', name);
					$(document).trigger('modal:hide', $('#projectModal'));
				});

			});


	})

// New project
	$('.new').click(function(){
		$('.new .namerow').addClass('open');
	});

	$('.new .button').click(function(){
		$('.valifail').removeClass('show');

		var name= $('.new').find("input[type='text']").val();
		if (name==''){
			$('.valifail').addClass('show');
		}else{
			window.location.href= '/project/make/' + name;
		}
	});
	

});