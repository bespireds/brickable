function wired()
{

	$('[data-connect]').each(function(){ 

		var mynameis  = $(this).attr('data-connect');
		var modelname = '[data-name="#"]'.replace('#',mynameis);
		if ($(modelname).length > 0 )
		{
			t1= $(modelname).offset().top + 16;
			l1= $(modelname).offset().left;
			h1= $(modelname).height();

			t2= $(this).offset().top  + 10;
			l2= $(this).offset().left + $(this).width();
			w2= $(modelname).width();
	
			var id= '#' + $(this).attr('data-wire');


			wires(id, Math.floor(t1),Math.floor(l1),Math.floor(t2),Math.floor(l2), h1,w2);
		}

	});

}

function wires(id, t1,l1,t2,l2, h1, w2)
{

	if (( l1 - l2 > -w2 ) && ( l1- l2 < 20 )) {

		if ( t2 > t1 ){

			top_mid_wires(id, t1,l1,t2,l2, h1);

		}else{

			bottom_mid_wires(id, t1,l1,t2,l2);

		}

		return;
	
	}

	if ( l2 < l1 ){

		if ( t2 > t1 ){

			top_right_wires(id, t1,l1,t2,l2);

		}else{
		
			bottom_right_wires(id, t1,l1,t2,l2);
	
		}
	}else{
		if ( t2 > t1 ){

			top_left_wires(id, t1,l1,t2,l2);

		}else{

			bottom_left_wires(id, t1,l1,t2,l2);
			
		}
	}
}

function top_mid_wires(id, t1,l1,t2,l2, drop)
{
	l1 -= 40; l2 += 40;
	var w= l2-l1+20, h= t2-t1+8; drop -= 5;
	var ex= w-2, ey= h-7, eym= ey-40, exm= ex-40, eyd= drop+(ey-drop)/2;
	
	var exd= ex-40 ;
	$(id + ' svg').attr('width', w+'px').attr('height', h+'px').css({left: l1, top: t1});
	var path= 'M 40 5 C 0 5  0 ~drop~ 40 ~drop~ ';
	var path= path + 'L ~exd~ ~drop~ ';

	var path= path + 'C ~exd~ ~drop~ ~ex~ ~drop~ ~ex~ ~eyd~ ';
	var path= path + 'C ~ex~ ~eyd~ ~ex~ ~ey~ ~exm~ ~ey~ ';

	path = path.replace(/~drop~/g, drop).replace(/~eyd~/g, eyd).replace(/~ey~/g, ey);
	path = path.replace(/~ex~/g, ex).replace(/~exm~/g, exm).replace(/~eym~/g, eym);
	path = path.replace(/~exd~/g, exd);

	$(id + ' svg path').attr('d', path);
//	$(id + ' svg circle').attr('cx', exd).attr('cy', drop);

}

function bottom_mid_wires(id, t1,l1,t2,l2)
{
	l1 -= 40; l2 += 40;
	var w= l2-l1+20, h= t1-t2+8;
	var sx= 18, ex= w-10, sy= 2, ey= h-4;
	var ds = h/3;

	$(id + ' svg').attr('width', w+'px').attr('height', h+'px').css({left: l1, top: t2});
	var path= 'M 40 ~sy~ C ~sxm~ ~sy~   ~sxm~ ~myb~   ~mx~ ~my~   ~exm~ ~myt~   ~exm~ 1   ~ex~ 1';

	path = path.replace(/~sx~/g, sx).replace(/~sy~/g, h);
	path = path.replace(/~sxm~/g, sx-20).replace(/~exm~/g, ex+20);
	path = path.replace(/~ex~/g, w-40);
	path = path.replace(/~mx~/g, w/2).replace(/~my~/g, h/2);
	path = path.replace(/~myb~/g, h/2+ds).replace(/~myt~/g, h/2-ds);

	$(id + ' svg path').attr('d', path);
}


function bottom_left_wires(id, t1,l1,t2,l2)
{
	var w= l2-l1-120, h= t1-t2+10;
	sx= 40; ex= w-40; sy= 2; ey= h-10; h2 = Math.floor(h * 2/ 3);
	var path= 'M ~sx~ ~ey~ C ~sxL~ ~ey~, ~sxL~ ~sy~ , ~exL~ ~sy~ L ~ex~ ~sy~';
		path = path.replace(/~sx~/g,  sx).replace(/~ey~/g, ey);
		path = path.replace(/~sxL~/g, sx - 40).replace(/~eyU~/g, ey-h2);
		path = path.replace(/~ex~/g,  ex).replace(/~sy~/g,  sy);
		path = path.replace(/~exL~/g, sx + (w/2));

	if ( w > 0 ){
		$(id + ' svg').attr('width', w+'px').attr('height', h+'px').css({left: l1- 40 , top: t2});
	}
	$(id + ' svg path').attr('d', path);
}

function top_left_wires(id, t1,l1,t2,l2)
{
	var w= l2-l1-140, h= t2-t1+10;
	sx= 40; ex= w; sy= 2; ey= h-10; h2 = Math.floor(h * 2 / 3);
	var path= 'M ~sx~ ~sy~ C ~sxL~ ~sy~, ~sxL~ ~ey~ , 60 ~ey~ L ~ex~ ~ey~';
		path = path.replace(/~sx~/g,  sx).replace(/~ey~/g, ey);
		path = path.replace(/~sxL~/g, sx-40).replace(/~eyU~/g, ey-h2);
		path = path.replace(/~ex~/g,  ex).replace(/~sy~/g,  sy);
	if ( w > 0 ){
		$(id + ' svg').attr('width', w+'px').attr('height', h+'px').css({left: l1- 40, top: t1});
	}
	$(id + ' svg path').attr('d', path);
}

function bottom_right_wires(id, t1,l1,t2,l2)
{
	var w= l1-l2+10, h= t1-t2+8;
	sx= 18; ex= w-10; sy= 2; ey= h-4; 
	var d= (ex-sx) / 2;

	$(id + ' svg').attr('width', w+'px').attr('height', h+'px').css({left: l2, top: t2});
	var path= 'M ~sx~ ~sy~ C ~sx10~ ~sy~ , ~ex10~ ~ey~ , ~ex~ ~ey~';
	path = path.replace(/~sx~/g,   sx).replace(/~sx10~/g, sx+d).replace(/~d~/g, d);
	path = path.replace(/~ex10~/g, ex-d).replace(/~ex~/g,   ex).replace(/~ex5~/g, ex-20);
	path = path.replace(/~sy~/g,   sy).replace(/~sy10~/g, sy+d);
	path = path.replace(/~ey10~/g, ey-d).replace(/~ey~/g,   ey);
	$(id + ' svg path').attr('d', path);
}

function top_right_wires(id, t1,l1,t2,l2)
{
	var w= l1-l2+10, h= t2-t1+10;
	sx= 18; ex= w-10; sy=  h-8; ey= 4;
	var d= (ex-sx) / 2;

	$(id + ' svg').attr('width', w+'px').attr('height', h+'px').css({left: l2, top: t1});
	var path= 'M ~sx~ ~sy~ C ~sx10~ ~sy~ , ~ex10~ ~ey~ , ~ex~ ~ey~';
	path = path.replace(/~sx~/g,   sx).replace(/~sx10~/g, sx+d).replace(/~d~/g, d);
	path = path.replace(/~ex10~/g, ex-d).replace(/~ex~/g,   ex).replace(/~ex5~/g, ex-20);
	path = path.replace(/~sy~/g,   sy).replace(/~sy10~/g, sy+d);
	path = path.replace(/~ey10~/g, ey-d).replace(/~ey~/g,   ey);
	$(id + ' svg path').attr('d', path);
}


