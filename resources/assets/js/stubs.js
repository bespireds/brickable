function getStub(what)
{
	var html = '';
	switch(what)
	{
		case 'modal-model':
			html += '<div class="header">Loading...<a class="close-button right" data-close-modal="" aria-label="Close"><i class="icon-cancel"></i></a></div>';
			html += '<ul><li class="has right"><span><a class="button grey" data-model-sure=""><i class="icon-trash"></i>Delete</a></span><a class="button grey" data-close-modal=""><i class="icon-cancel"></i>Cancel</a><a href="" class="button"><i class="icon-ok"></i>Save</a></li></ul>';
		break;
		case 'modal-module':
			html += '<div class="header">Loading...<a class="close-button right" data-close-modal="" aria-label="Close"><i class="icon-cancel"></i></a></div>';
			html += '<ul><li class="has right"><span><a class="button grey" data-project-sure=""><i class="icon-trash"></i>Delete</a></span><a class="button grey" data-close-modal=""><i class="icon-cancel"></i>Cancel</a><a href="" class="button"><i class="icon-ok"></i>Save</a></li></ul>';
		break;
		case 'modal-config':
			html += '<div class="header">Loading...<a class="close-button right" data-close-modal="" aria-label="Close"><i class="icon-cancel"></i></a></div>';
			html += '<ul><li class="has right"><span><a class="button grey" data-model-sure=""><i class="icon-trash"></i>Delete</a></span><a class="button grey" data-close-modal=""><i class="icon-cancel"></i>Cancel</a><a href="" class="button"><i class="icon-ok"></i>Save</a></li></ul>';
		break;

		case 'html-field':
			html = '<li class="[typecol]" [connect]><span>[name]</span><span class="[narrow]">[reltype]</span></li>';
		break;

		case 'html-model':
			html+= '<div class="model [type]" ';
			html+= '    data-id="[id]" data-name="[mname]" ';
			html+= '    style="top:[y]px; left: [x]px; z-index: [zidx]">';
			html+= '    <div class="header">[name]<a href="" data-model-edit="[id]" class="right"><i class="view-edit-box"></i></a></div>';
			html+= '    <ul class="body"></ul>';
			html+= '</div>';
		break;
		case 'update-header':
			html+= '    [name]<a href="" data-model-edit="[id]" class="right"><i class="view-edit-box"></i></a>';
		break;		
	}
	return html;

}