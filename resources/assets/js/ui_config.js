
$(function() {

	var project_id= $('#models').attr('data-project-id');

// Sidebar
	$('[data-action="add-config"]').click( function(e){

		$('#editModal').html(getStub('modal-config')).trigger('modal:show', '#editModal');
		$.ajax("/model/async/{id}/config".replace('{id}', $('[data-project-id]').attr('data-project-id') ))
			.done(function(response){ 
				$('#editModal').html(response); 
			});
	});


	$(document).on('click', '[data-save-config]', function(e){
		
		var form     = $('#editModal').find('form');
		var model_id = form.attr('data-model-id');
		if ( model_id == '' ) model_id = 0;
		var url      = "/model/async/{id}/update".replace('{id}', model_id);

		$('#editModal ul').hide();
		$('#editModal .header input').hide();
		$('#editModal .header').append('Saving...');
		
		$.ajax( { url:url, type: "POST", data: form.serialize() })
			.done(function(response){ 
					
				modelreplace(response.id); 

			});
	
	});

});