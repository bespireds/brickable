
$(function() {


	var project_id= $('#models').attr('data-project-id');

// Sidebar
	$('[data-action="add-module"]').click( function(e){

		$('#editModal').html(getStub('modal-module')).trigger('modal:show', '#editModal');
		$.ajax("/module/async/{id}/edit".replace('{id}', $('[data-project-id]').attr('data-project-id') ))
			.done(function(response){ 
				$('#editModal').html(response); 
			});
	})


// Modals

	$(document).on('click', '[data-delete]', function(e){
		var $li= $(this).closest('li');
		if ( $(this).next().hasClass('open') ){
			setTimeout( function(){ $li.addClass('locked'); }, 50 );
			$(this).next().removeClass('open');
		}else{
			$li.addClass('locked');
			$(this).next().addClass('open');
		}
	});
	
	$(document).on('click', '[data-close-sure]', function(e){
		var $li= $(this).closest('.sure');
			$li.removeClass('open');
	});


	$(document).on('click', '[data-save-module]', function(e){
		
		var form     = $('#editModal').find('form');
		var url      = "/module/async/{id}/update".replace('{id}', project_id);

		$('[name*="module_label"]').each( function(){
			if ($(this).val()=='') $(this).val('Noname'); 
		});

		$('#editModal ul').hide();
		$('#editModal .header input').hide();
		$('#editModal .header').append('Saving...');
		
		$.ajax( { url:url, type: "POST", data: form.serialize() })
			.done(function(response){ 
					
				$(document).trigger('modal:hide', $('#editModal'));				

			});
	
	});


});