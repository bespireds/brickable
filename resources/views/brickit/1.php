<?php



if (in_array('--help',$argv)){
	echo "options:\n--cleanup\n--skip-migrations\n--skip-models\n--skip-requests\n--skip-controllers\n--skip-paths\n\n";
	exit;
}

if (in_array('--cleanup',$argv)){
	do_cleanup();
	exit;
}

$re = "/()-([0-9]*?)-([0-9]*?).php|.php/"; 
$phpname = preg_replace($re, "", $argv[0]).'.php';
$newname= str_replace('.php', '', $phpname) . '-' . date("Ymd-His") . '.php';
rename( $argv[0], $newname );
 
// create Folders
foreach (mkdirs() as $name => $mode) {
	if (!file_exists($name)){
		$path = getcwd() . '/' . $name;
		shell_exec ( "mkdir -p \"$path\"" );
	} 
}
// create Configs
foreach (configs() as $colon => $table) {

	if ( strpos($colon,":") !== false ) make_config($colon, $table);
	
} 
// create Controllers
if (!in_array('--skip-controllers',$argv)){
	foreach (models() as $colon => $path) { // for now with models

		if ( strpos($colon,":") !== false ){
			$name  = str_replace(":", "_", $colon);
			$path  = colon2model($colon);
			
			$controller          = "php artisan make:controller {$path}Controller --resource";
			echo "$controller \n";
			$shell  = shell_exec ( $controller );

			$root = '/app/Http/Controllers/';
			moveApp($root, $path.'Controller.php');

			$filename = getcwd() . $root . $path . 'Controller.php';
			preg_match("/([\\S]*):([\\S]*)/", $colon, $matches);
			echo "adjust controller: ".$filename ."\n";

			$class= str_replace('/', '\\', $path);
			$mod= $matches[1];
			$var= $matches[2];
			inject($filename, ['Requests','Controller;'], "use App\\Data\\Models\\{$class};\n" );
			inject($filename, ['function index(',   '//'], controller_make_index($mod, $var)   );
			inject($filename, ['function create(',  '//'], controller_make_create($mod, $var)  );
			inject($filename, ['function edit(',    '//'], controller_make_edit($mod, $var)    );
			inject($filename, ['function show(',    '//'], controller_make_show($mod, $var)    );
			inject($filename, ['function store(',   '//'], controller_make_store($mod, $var)   );
			inject($filename, ['function update(',  '//'], controller_make_update($mod, $var)  );
// 			inject($filename, ['function destroy(', '//'], controller_make_destroy($matches[1], $matches[2]) );
		}
	}
} 
// create Requests
if (!in_array('--skip-requests',$argv)){
// 	for now with models
	foreach (models() as $colon => $table) {
		if ( strpos($colon,":") !== false ){
			$path = colon2model($colon);
			$request            = "php artisan make:request {$path}Request";
			echo "$request \n";
			$shell  = shell_exec ( $request );

			$root = '/app/Http/Requests/';
			moveApp($root, $path.'Request.php');

			$filename= getcwd() . $root.$path . 'Request.php';
			inject($filename, ['public function authorize','{'], "\t\treturn true;" );
			inject($filename, ['rules()','//'], request_fillables($table) );
		}
	}
} 
// create Migrations
if (!in_array('--skip-migrations',$argv)){
	foreach (models() as $colon => $table) {
		if ( strpos($colon,":") !== false ) make_migration_create($colon, $table);
		if ( strpos($colon,"*") !== false ) make_migration_update($colon, $table);
	}
} 
// create Models
if (!in_array('--skip-models',$argv)){
	foreach (models() as $colon => $table) {

		if ( strpos($colon,":") !== false ) make_model_create($colon, $table);
		// create Model one-to-many relation
		if ( strpos($colon,"*") !== false ) make_model_relation($colon, $table);
	}
} 
// create Paths
if (!in_array('--skip-paths',$argv)) {
// 	for now with models
	foreach (models() as $colon => $path) {  
		if ( strpos($colon,":") !== false ){
			$re = "/([\\S]*):([\\S]*)/"; 
			preg_match($re, $colon, $matches);
			$path= $matches[1];
			$file= $matches[2];

			$class  = colon2model($colon);
			$to_inject= routefile($path, $file, $class);

			$filename = app_path("/Http/routes/{$path}.php");

			if (!file_exists($filename)) {
				$prepend = "\n\n";		
			}else{
				$prepend= file_get_contents($filename);
			}
			file_put_contents($filename, $prepend.$to_inject);
			echo "path $filename \n";

			// Add route/routes.php to routes.php
			$string = "include __DIR__ . '/routes/{$path}.php';";
			$checkfile= file_get_contents(app_path('Http/routes.php'));	
			if ( strpos($checkfile, $string) === false )
			{
				inject( app_path('Http/routes.php'), ['', "\n"], $string );
			}
		}
	}
} 
// create Views
if (!in_array('--skip-views',$argv)){
	foreach (views() as $colon => $table) {
		if ( strpos($colon,":") !== false ){
			make_view_path($colon);
			make_views($colon, $table);
		}	 
	}
}

exit;


function laravel_auth()
{
// 	rename
// 	resources/views/layouts/auth.blade.php

// 	@extends('layouts.auth') in 
// 	resources/views/auth/login.blade.php
// 	resources/views/auth/register.blade.php
// 	resources/views/auth/emails/password.blade.php
// 	resources/views/auth/passwords/email.blade.php
// 	resources/views/auth/passwords/reset.blade.php

	$css= ".container{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.navbar{position:relative;min-height:50px;margin-bottom:20px;border:1px solid transparent}.navbar-brand{float:left;height:50px;padding:15px;font-size:18px;line-height:20px}.navbar-right{float:right!important}.navbar-default{background-color:#f8f8f8;border-color:#e7e7e7}ul.nav{margin-bottom:.3rem}.nav>li{position:relative;display:block}.navbar-nav>li{float:left}.navbar-nav>li>a{padding-top:15px;padding-bottom:15px;line-height:20px;position:relative;display:block;padding:10px 15px}.panel-body{padding:15px}.col-md-4{width:33.33333333%}.col-md-8{width:66.66666667%}.col-md-offset-2{margin-left:16.66666667%}.btn{box-sizing:border-box;display:inline-block;padding:6px 12px;margin-bottom:0;font-size:14px;font-weight:400;line-height:1.42857143;text-align:center;white-space:nowrap;vertical-align:middle;-ms-touch-action:manipulation;touch-action:manipulation;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;background-image:none;border:1px solid transparent;border-radius:4px}.btn-primary{color:#fff;background-color:#337ab7;border-color:#2e6da4}";
	file_put_contents(public_path('css/auth.css'), $css);
}

function do_cleanup()
{
	// models
	deltree(getcwd().'/app/Data/');

	// controllers
	$root= getcwd().'/app/Http/Controllers/';
	foreach( scandir($root) as $name) {
		if ( substr($name,0,1)!='.') {
			$path= $root.$name;
			if ( is_dir($path) )
			{
				if (!in_array($name, ['Auth'])) {
					deltree($path);
				}
			}
		}
	}

	// requests
	$root= getcwd().'/app/Http/Requests/';
	foreach( scandir($root) as $name) {
		if ( substr($name,0,1)!='.') {
			$path= $root.$name;
			if ( is_dir($path) )
			{
				if (!in_array($name, ['Auth'])) {
					deltree($path);
				}
			}
		}
	}

	// routes
	$root= getcwd().'/app/Http/routes/';
	foreach( scandir($root) as $name) {
		if ( substr($name,0,1)!='.') unlink($root.$name);
	}
	
	// route file
	$filename = getcwd().'/app/Http/routes.php';
	$fd= file_get_contents($filename);
	$fd= "\n\n" . substr($fd, strpos($fd, '/*'));
	file_put_contents($filename, $fd);

	// migration
	$root= getcwd().'/database/migrations/';
	foreach( scandir($root) as $name) {
		if ( substr($name,0,1)!='.') {
			if ( intval(substr($name,0,4)) > 2015 ) unlink($root.$name);
		}
	}

	// config
	$root= getcwd().'/config/';
	foreach( scandir($root) as $name) {
		if ( substr($name,0,1)!='.') {
			$path= $root.$name;
			if ( is_dir($path) )
			{
				if (!in_array($name, ['auth','errors','layouts','vendor'])) {
					deltree($path);
				}
			}
		}
	}

	// view
	$root= getcwd().'/resources/views/';
	foreach( scandir($root) as $name) {
		if ( substr($name,0,1)!='.') {
			$path= $root.$name;
			if ( is_dir($path) )
			{
				if (!in_array($name, ['auth','errors','layouts','vendor'])) {
					deltree($path);
				}
			}
		}
	}
}

function delTree($dir) { 
	if (!file_exists($dir)) return;
	$files = array_diff(scandir($dir), array('.','..')); 
		foreach ($files as $file) { 
			(is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
		} 
	return rmdir($dir); 
} 
// *** === CONFIG HELPERS  === ***
function make_config($colon, $data)
{
	$re = "/([\\S]*):([\\S]*)/"; 
	preg_match($re, $colon, $matches);
	
	$path= getcwd() . '/config/' . $matches[1];
	$file= $matches[2].'.php';
	
	if (!file_exists( $path )) mkdir( $path );
	$filename = $path . '/' . $file;
	$filedata = config_file($data);

	file_put_contents($filename, $filedata);
	
	return;
}

function config_file($data)
{
	$stub =[];
	$stub[]= '';
	$stub[]= '';
	$stub[]= 'return [';
	foreach ($data as $key => $value) {
		$stub[]= "    '$key' => \"$value\",";
	}
	$stub[]= '];';

	return join("\n", $stub);
}
// *** === CONTROLLER HELPERS  === ***

function controller_make_index($mod, $var){	
	$view = $mod.'.'.$var.'.index';
	$class= ucfirst($var);
	return "\t\treturn view('$view')->with('items', $class::all() );";
}

function controller_make_create($mod, $var){
	$view = $mod.'.'.$var.'.create';
	return "\t\treturn view('$view');";
}

function controller_make_show($mod, $var){
	$class= ucfirst($var);
	$view = strtolower($mod.'.'.$var.'.show');
	return "\t\t\${$var} = $class::find(\$id);\n\t\treturn view('$view')->with('{$var}', \${$var});";
}

function controller_make_edit($mod, $var){
	$class= ucfirst($var);
	$view = strtolower($mod.'.'.$var.'.edit');
	return "\t\t\${$var} = $class::find(\$id);\n\t\treturn view('$view')->with('{$var}', \${$var});";
}

function controller_make_store($mod, $var){
	$path = $mod.'.'.$var.'.index';
	$class= ucfirst($var);
	$php = '';
	$php.= "\t\t// Please insert Validation \n";
    $php.= "\t\t$class::create(\$request->all());\n";
    $php.= "\t\treturn redirect()->route('{$path}');";
    return $php;
}

function controller_make_update($mod, $var){
	$path = $mod.'.'.$var.'.index';
	$class= ucfirst($var);
	$php = '';
	$php.= "\t\t\${$var}= {$class}::find(\$id);	\n";
	$php.= "\t\t// Please insert Validation \n";
//	$this->validate($request, [
//  	'title' => 'required|unique|max:255',
//      'body'  => 'required',
//   ]);
	$php.= "\t\t\${$var}->update(\$request->all());	\n";
	$php.= "\t\treturn redirect()->route('{$path}'); \n";
	$php.= "\n\t\t// consider: function update({$class}Request \$request, {$class} \${$var}) \n";
	$php.= "\t\t// this would need a {$class}Request and a corrected routing. \n";

	return $php;
}

function controller_make_destroy($mod, $var){
	return '';
}

// *** === FILE HELPERS  === ***

function moveApp($root, $file)
{
	if ( substr($file, 0, 4) == 'App/' )
	{
		$made = ltrim( str_replace('App/', '', $file) , '/');
		if ( file_exists( getcwd() . $root . $made ) )
		{
			rename (getcwd() . $root . $made, getcwd() . $root . ltrim($file, '/') );
		}
	}
}

function inject($filename, $spot, $inject)
{

	if (!file_exists($filename)) return;
	if ($inject == '') return;
	
	$file= file_get_contents($filename);
	if (strpos( $file, $inject ) !== false ) return; 

	$pos = 0;
	foreach ($spot as $find) {
		$pos= strpos($file, $find, $pos) + strlen($find);
	}
	$file = substr($file, 0, $pos) . "\n" . $inject . substr($file, $pos);
	$file = preg_replace("/([\\ ]{4})/", "\t", $file);
	file_put_contents($filename, $file);

}

function app_path( $path = '' )
{
	return getcwd() . '/app/' . ltrim($path, '/');	
}

function public_path( $path = '' )
{
	return getcwd() . '/public/' . ltrim($path, '/');	
}

function view_path( $path = '' )
{
	return getcwd() . '/resources/views/' . ltrim($path, '/');	
}

function migration_path( $file = '', $root = '/database/migrations/' )
{
	if ( substr($file, 0, 18) == 'Created Migration:') $file= rtrim(substr($file,19));
	if ( ($file != '' ) && (substr($file,-4) != '.php')) $file .= '.php';
	return getcwd() . $root . ltrim($file, '/');
}

function model_path( $file = '', $root = '/app/Data/Models/')
{
	if ( substr($file, 0, 14) == 'Created Model:') $file= rtrim(substr($file,15));
	if ( ($file != '' ) && (substr($file,-4) != '.php')) $file .= '.php';
	// Weird Laravel error where scaffolding can't take place in an App folder... Argh.
	moveApp($root, $file);
	return getcwd() . $root . ltrim($file, '/');
}




// *** === MIGRATE HELPERS  === ***

function make_migration_create($colon, $table)
{
	$name  = str_replace(":", "_", $colon);
	$path  = colon2model($colon);

	$migrate            = "php artisan make:migration create_$name";
	echo "$migrate \n";
	$shell = shell_exec ( $migrate );

	$filename = migration_path($shell);
	$migrations[$name] = $filename;
	inject($filename, ['up()','//'], schema_create($name, blue2columnize($table)) );
	inject($filename, ['down','//'], schema_drop($name) );
}

function make_migration_update($colon, $table)
{
	$name  = str_replace("*", "_", $colon);
	$path  = colon2model($colon);

	$migrate            = "php artisan make:migration update_$name";
	echo "$migrate \n";
	$shell = shell_exec ( $migrate );

	$filename = migration_path($shell);
	inject($filename, ['up()','//'], schema_update($name, blue2columnize($table)) );
//	inject($filename, ['down','//'], schema_dropkey($name) );
}
// *** === MODEL HELPERS  === ***
function make_model_create($colon, $table)
{
	global $models;
	$name  = str_replace(":", "_", $colon);
	$path  = colon2model($colon);
	
	$model              = "php artisan make:model Data/Models/$path";
	echo "$model \n";
	$shell      = shell_exec ( $model );

	$filename = model_path( $path );
	$models[$name] = $filename;

	inject($filename, ['Model','//'], model_fillables($table) );
	inject($filename, ['Model','//'], model_create($name) );

}

function make_model_relation($colon, $table)
{
	global $models;
	$name  = str_replace("*", "_", $colon);
	$fname = substr($colon, 1 + strpos($colon, '*'));
	$path  = colon2model($colon);
	// int=app_campaign_id,uns;foreign=app_campaign_id,ref=id,on=app_campaign;

	$re = "/on=([\\S]+?);/"; 
	preg_match($re, $table, $matches);
	$model = $matches[1];
	
	$re = "/foreign=([\\S]+?),/"; 
	preg_match($re, $table, $matches);
	$id = $matches[1];

	// has many in related model
	$filename = $models[$model];
	$relation = str_replace('/', '\\', 'App/Data/Models/'.$path );
	inject($filename, ['Model','ELOQUENT'], model_hasMany( $relation, plural($fname), $id));

	// belongs to in this model	
	$filename = $models[$name];
	$path  = colon2model($model);
	$relation = str_replace('/', '\\', 'App/Data/Models/'.$path );
	inject($filename, ['Model','ELOQUENT'], model_belongsTo( $relation, functionname($model) ));

}


function colon2model($name)
{
	$parts = explode(":", $name);
	if( count(explode("*", $name)) > count($parts) ) $parts= explode("*", $name);
	if( count(explode("_", $name)) > count($parts) ) $parts= explode("_", $name);

	foreach ($parts as $key => $part) {
		$parts[$key]= str_replace(" ", "", 
			ucwords(str_replace("_", " ", $part))
		);
	}
	$path  = join('/', $parts);
	return $path;
}


function model_create($name) 
{
	$name = plural($name);
	return "\tprotected \$table = '$name';\n\n\t//";
}


function model_fillables($table)
{
//'app:phase' => 'inc;txt=name;int=of_p_config,uns;foreign=of_p_config,ref=id,on=cnf_phaseconfig;timestamps;',
	$re = "/(txt|int|chr|boo)=([\\S]+?)(;|,)/";
	preg_match_all($re, $table, $matches);

	if ( !isset($matches) ) return '';

	$fill = "\tprotected \$fillable = [\n";
	foreach ($matches[2] as $varname) 
	{
		$fill.= "\t\t'".$varname."',\n";
	}
	$fill.= "\t];\n\n\t// ELOQUENT";
	return $fill;
}
// BelongsTo
// BelongsToMany
// HasMany
// HasManyThrough
// HasOne
// HasOneOrMany
// MorphMany
// MorphOne
// MorphOneOrMany
// MorphPivot
// MorphTo
// MorphToMany
// Pivot
// Relation

//	public function campaignmembers()
//	{
//		return $this->hasMany('App\Data\Models\App\Campaignmembers','app_campaign_id')->orderby('order');
//	}
function model_hasMany($path, $model, $table)
{
	$func = "\tpublic function $model()\n";
	$func.= "\t{\n";
	$func.= "\t\treturn \$this->hasMany('$path','$table');\n";
	$func.= "\t}\n";
	return $func;	
}

function model_belongsTo($path, $model)
{
	$func = "\tpublic function $model()\n";
	$func.= "\t{\n";
	$func.= "\t\treturn \$this->belongsTo('$path');\n";
	$func.= "\t}\n";
	return $func;	
}
// *** === REQUEST HELPERS  === ***

function request_fillables($table)
{
	//'app:phase' => 'inc;txt=name;int=of_p_config,uns;foreign=of_p_config,ref=id,on=cnf_phaseconfig;timestamps;',
	$re = "/(txt)=([\\S]+?)(;|,)/";
	preg_match_all($re, $table, $matches);

	if ( !isset($matches) ) return '';

	$fill = "";
	foreach ($matches[2] as $key => $varname) 
	{
		$fill.= "\t\t\t'".$varname."' => ['required'],\n";
	}
	return $fill;
}
// *** === ROUTE HELPERS  === ***

function routefile($path, $file, $class)
{
	$item   = plural($file);

	$root= [];
	$root['index']  = "{$path}/{$file}";                $rest['index']  =  'get   ';  $meth['index']  = 'index';
	$root['create'] = "{$path}/{$file}/create";         $rest['create'] =  'get   ';  $meth['create'] = 'create';
	$root['store']  = "{$path}/{$file}";                $rest['store']  =  'post  ';  $meth['store']  = 'store';
	$root['show']   = "{$path}/{$file}/{{$item}}";      $rest['show']   =  'get   ';  $meth['show']   = 'show';
	$root['edit']   = "{$path}/{$file}/{{$item}}/edit"; $rest['edit']   =  'get   ';  $meth['edit']   = 'edit';
	$root['update'] = "{$path}/{$file}/{{$item}}";      $rest['update'] =  'put   ';  $meth['update'] = 'update';
	$root['delete'] = "{$path}/{$file}/{{$item}}";      $rest['delete'] =  'delete';  $meth['delete'] = 'destroy';

	$class = str_replace('/', '\\', $class);

	$l= 25;
	foreach ($root as $v) { $l = max( $l, strlen($v)); }
	// currently 1 guard: web
	$routes.= "Route::group(['middleware' => ['web']], function () {\n";

	foreach ($root as $key => $value) {
		$restfull = $rest[$key];
		$method   = $meth[$key];
		$uses     = substr("'{$class}Controller@$method'            ", 0, strlen($class) + 20 );
		$aspath   = substr("'{$path}.{$file}.$method'      ", 0, strlen($path) + strlen($file) + 12 );
		$request  = substr("'" . $value . "'" . str_repeat(' ',$l) , 0, $l + 2 );

		$routes.= "\tRoute::$restfull ($request, ['as' => $aspath , 'uses' => $uses ]); \n";

	}
	$routes.= "});\n";

	return $routes;
}

function write_routefile($path, $class)
{
	$data= routefile($path, $class);
	$filename = $path . '.php';
	$filepath = app_path('Http/routes/');
	file_put_contents($filepath . $filename, $data );
	
	return $filename;
}

// *** === SCHEMA HELPERS  === ***

function functionname($colom)
{
	$parts= explode('_', $colom);
	return $parts[1];
}

function plural($name)
{
	return rtrim($name, 's') . 's';
}


function schema_create($name, $table)
{
	$name  = plural($name);

	$schema  = "\t\tSchema::create('$name', function(Blueprint \$table){\n";
	foreach ($table as $column) {
		if ( key($column) != '' ) $schema .= "\t\t\t" . columnize($column) ."\n";
	}
	$schema .= "\t\t});\n";
	return $schema;
}

function schema_update($name, $table)
{
	$name  = plural($name);

	$schema  = "\t\tSchema::table('$name', function(Blueprint \$table){\n";
	foreach ($table as $column) {
		if ( key($column) != '' ) $schema .= "\t\t\t" . columnize($column) ."\n";
	}
	$schema .= "\t\t});\n";
	return $schema;
}

function schema_drop($name)
{
	$name = rtrim($name, 's') . 's';
	$schema  = "\t\tSchema::drop('$name');\n";
	return $schema;
}


function columnize($column) 
{
	if ( key($column) == '' ) return '';
	$define= '$table';
	foreach ($column as $type => $value) {
		if ( $type == 'ref' )  $type = 'references';
		if ( $type == 'uns' )  $type = 'unsigned';
		if ( $type == 'idx' )  $type = 'index';		
		if ( $type == 'inc' )  $type = 'increments';
		if ( $type == 'del' )  $type = 'onDelete';

		if ( $type == 'def' )  $type = 'default';

		if ( $type == 'chr' ) $value = str_replace(":", ",", $value);
		if ( $type == 'chr' )  $type = 'char';
		if ( $type == 'txt' )  $type = 'text';
		if ( $type == 'int' )  $type = 'integer';
		if ( $type == 'boo' )  $type = 'boolean';
		if ( $type == 'flo' )  $type = 'float';

		if ( $type == 'dat' )  $type = 'date';
		if ( $type == 'dti' )  $type = 'dateTime';
		if ( $type == 'tim' )  $type = 'time';

		if ( $type == 'on' )   $value = plural($value);

		if ( $value == '' ) $define.= '->' . $type . '()';
		if ( $value != '' ) $define.= '->' . $type . "('$value')";
	}
	$define.= ';';
	return $define;
}

// $name  = 'group_page';
// $blue  = inc; int=page_id,uns,idx; int=group_id,uns,idx; 
//			foreign=page_id,ref=id,on=pages,del=cascade; foreign=group_id,ref=id,on=pages,del=cascade;
//			timestamps;

// $table = [
// 	[ 'inc'=>'id', 'uns'=>'' ],
// 	[ 'int'=>'page_id',  'uns'=>'', 'idx'=>''],
// 	[ 'int'=>'group_id', 'uns'=>'', 'idx'=>''],
// 	[ 'foreign'=>'page_id',  'ref' =>'id', 'on' =>'pages',  'del'=>'cascade' ],
// 	[ 'foreign'=>'group_id', 'ref' =>'id', 'on' =>'groups', 'del'=>'cascade' ],
// 	[ 'timestamps' => '']
// ];
function blue2columnize($blue)
{
	$blue= str_replace('inc;', 'inc=id,uns;', $blue);

	$defines= [];
	foreach( explode(';', $blue) as $row ){
		$def= [];
		foreach(explode(',',$row) as $define){
			if (substr($define, 0, 8) != 'foreign:'){
				$pair= explode('=', $define);
				$def[trim($pair[0])]= (isset($pair[1])) ? trim($pair[1]) : '';
			}
		}
		$defines[]= $def;
	}	
	return $defines;
}
// *** === VIEW HELPERS === ***
function make_view_path($colon)
{
	$re = "/([\\S]*):([\\S]*)/"; 
	preg_match($re, $colon, $matches);
	$path= $matches[1];
	$file= $matches[2];

	if (!file_exists(view_path($path))) mkdir(view_path($path));
	if (!file_exists(view_path($path.'/'.$file))) mkdir(view_path($path.'/'.$file));

}

function make_views($colon, $table){
	preg_match("/([\\S]*):([\\S]*)/", $colon, $matches);
	$title= ucwords($matches[2]);
	$item = $matches[2];
	$route= str_replace(':', '.', $colon);
	$path= view_path($matches[1].'/'.$matches[2].'/');

	$re = "/(text|date)=([\\S]*?);/"; 
	preg_match_all($re, $table, $fields);
	
	$stubs= create_stubs();
	
	// index
	$file= str_replace( 
		['~title~','~row~','~route~'], 
		[$title, make_index_rows($colon, $table), $route.'.create'],
		$stubs['form_index']);
	file_put_contents($path.'index.blade.php', $file);

	// create
	$file= str_replace( 
		[ '~title~','~formfields~','~routename~'],		
		[ $title, make_input_rows($table, 'create'), $route.'.store'], 
		$stubs['form_create']);
	file_put_contents($path.'create.blade.php', $file);

	//edit
	$file= str_replace( 
		['~title~','~formfields~','~routename~','~route~','~id~'],
		[$title, make_input_rows($table, 'input', $item ), $route.'.update', $route.'.index', $item.'->id'], 
		$stubs['form_edit']);
	file_put_contents($path.'edit.blade.php', $file);

	//show
	$file= str_replace( 
		['~title~','~formfields~'], 
		[$title, make_input_rows($table, 'show', $item)], 
		$stubs['form_show']);
	file_put_contents($path.'show.blade.php', $file);

}

function make_index_rows($colon, $table)
{
	$stubs= create_stubs();
	
	$re = "/([\\S]*):([\\S]*)/"; 
	preg_match($re, $colon, $matches);
	$path= $matches[1];
	$file= $matches[2];

	$re = "/(text|date)=([\\S]*?);/"; 
	preg_match_all($re, $table, $fields);

	$second= $fields[2][1];
	$first=  $fields[2][0];
	if (!isset($first))  $first= 'null';
	if (!isset($second)) $second= 'null';

	$input= $stubs['row_index'];
	$input= str_replace('~mod~', $path, $input);
	$input= str_replace('~var~', $file, $input);

	$input= str_replace('~first~',  $first, $input);
	$input= str_replace('~second~', $second, $input);
	
	return $input;
}

function make_input_rows($table, $type = 'input', $itemname = '')
{
	$stubs= create_stubs();
	
	$re = "/(text|date)=([\\S]*?);/"; 
	preg_match_all($re, $table, $fields);

	$formfields= [];
	foreach ($fields[2] as $key => $fieldname) {
//		$field= $fields[1][$key];
//		$input= $stubs[$type.'_'.$fields];

		$input= $stubs[$type.'_text'];

		$input= str_replace('~title~', $fieldname, $input);
		$input= str_replace('~name~',  $fieldname, $input);
		$input= str_replace('~item~',  $itemname,  $input);

		$formfields[]= $input;
	}
	return join("\n", $formfields);
}
// *** === VIEW STUB DATA  === ***
function create_stubs()
{
    $stubs=stubs();
	foreach ($stubs as $key => $stub) {
		$stubs[$key] = gzinflate(base64_decode(strtr($stub, '[]', '+/')));
	}
	return $stubs;
}

//*** === BRICKABLE DATA === ***//

function mkdirs()
{
	return [
		'app/Http/routes'      => '0777',
		'app/Data/Models'      => '0777',
		'app/Data/Configs'     => '0777',

		'app/Data/Models/App'       => '0777',
		'app/Http/Controllers/App'  => '0777',
		'app/Http/Requests/App'     => '0777',
	];
}

function configs()
{
	return [
/*	~~ Configs ~~ */
	];	
}

function models()
{
	return [
/*	~~ Models ~~ */
	];
}

function controllers()
{
	return [
/*	~~ Controllers ~~ For now same as Models */
	];
}

function views()
{
	return [
/*	~~ Views ~~ */
	];
}

function stubs()
{
	return [
/*	~~ Compressed views ~~ */
	];
}
