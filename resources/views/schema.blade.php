@extends('layouts.wires')

@section('content')
<section id="wires"></section>
<section id="models" data-project-id='{{ $project->id }}'></section>
@include('menus.modal')
@include('menus.uploadmodal')
@include('menus.editmodal')
@include('menus.projectmodal', ['project' => $project])
@include('menus.sidecontrols', ['project' => $project])
@endsection

