@extends('layouts.auth')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Login</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
						{!! csrf_field() !!}

						@include('formthemes.app.groups.input-email',    [ 'label' => 'E-Mail Address', 'name' => 'email'    ])
						@include('formthemes.app.groups.input-password', [ 'label' => 'Password',       'name' => 'password' ])
						@include('formthemes.app.groups.input-checkbox', [ 'label' => 'Remember Me',    'name' => 'remember' ])
						
						@include('formthemes.app.submit.linked', [ 
							'icon' => 'sign-in', 'label' => 'Login', 
							'link' => 'Forgot Your Password?', 'url' => url('/password/reset') 
						])						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection