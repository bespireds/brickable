@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {!! csrf_field() !!}

                        @include('formthemes.app.groups.input-text',     [ 'label' => 'Name',             'name' => 'name'     ])
                        @include('formthemes.app.groups.input-email',    [ 'label' => 'E-Mail Address',   'name' => 'email'    ])
                        @include('formthemes.app.groups.input-password', [ 'label' => 'Password',         'name' => 'password' ])
                        @include('formthemes.app.groups.input-password', [ 'label' => 'Confirm Password', 'name' => 'password_confirmation' ])
                        
                        @include('formthemes.app.submit.single', [  'icon' => 'user', 'label' => 'Register' ])                      

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
