<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Brickable</title>
	<link href="{{ url('css/brickle.css') }}" rel="stylesheet"> 
	
</head>
<body>
	@yield('content')
	<script src="{{ url('js/jquery-2.2.1.min.js') }}"></script>
	<script src="{{ url('js/jquery-ui.min.js') }}"></script>
	<script src="{{ url('js/brickle.js') }}"></script>
	@yield('script')
</body>
</html>
