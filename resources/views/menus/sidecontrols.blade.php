<section id="side_controls">
	<ul>
	@if(!isset($mainmenu))
		<li><a class="side_button" 
			title="Add Model" data-action="add-model"><i class="model-add-box"></i></a></li>
		<li><a class="side_button" 
			title="Add Config" data-action="add-config"><i class="config-add-box"></i></a></li>
		<li><a class="side_button" 
			title="Add Config" data-action="add-module"><i class="module-add-box"></i></a></li>
		<li><a class="side_button" 
			title="Grid" data-action="grid"><i class="grid-box"></i></a></li>
		<li><a class="side_button" 
			title="Screen" data-action=""><i class="box-box"></i></a></li>
		<li><a class="side_button" 
			title="Project" data-action="project"><i class="box-launch-box"></i></a></li>
		<li><a class="side_button" href="/project/brickit/{{ $project->id }}"
			title="Brickit" data-action="brickit"><i class="brick-code-box"></i></a></li>
		<li><a class="side_button" href="/project/export/{{ $project->id }}"
			title="Download" data-action="download"><i class="export-box"></i></a></li>
		<li><a class="side_button" href="/"
			title="Exit" data-action="exit"><i class="reject-box"></i></a></li>
		<li><a class="side_button" href="" title="ZoomIn"  data-action="zoom-in"><i class="zoom-out-box"></i></a>
		    <a class="side_button" href="" title="ZoomOut" data-action="zoom-out"><i class="zoom-in-box"></i></a>
		</li>
	@endif
		<li class="bottom">
			<label class="upload">
				<form id="file-form" action="" method="POST">
    			<input type="file" id="file-select" name="files[]" multiple/>
  				<span class="side_button" ><i class="import-up-box"></i></span>
  				</form>
			</label>
		</li>		
	</ul>

</section>