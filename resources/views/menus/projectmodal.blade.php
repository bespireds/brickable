<div class="reveal subModal" id="projectModal" data-project-name="{{ $project->name }}" data-reveal >
	<div class="header">
	<span class="project">
		<input type="text" name="projectname" placeholder="{{ $project->name }}" value="{{ $project->name }}">
	</span>
	<span class="right">
		<a class="close-button right" data-close-modal><i class="box-cancel"></i></a>
	</span>
	</div>
	<ul><li></li></ul>
	<ul>
		<li class="has right">
			<a class="button grey" data-close-modal><i class="box-cancel"></i>Cancel</a>
			<a class="button" data-save-project><i class="box-ok"></i>Save</a>
		</li>
	</ul>
<script>
	document.title = "{{ $project->name }}";
</script>
</div>