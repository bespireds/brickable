<div class="reveal subModal" id="uploadModal" data-reveal>
	<div class="header">
	<span>
		Upload
	</span>
	<span class="right">
		<a class="close-button right" data-close-modal=""><i class="box-cancel"></i></a>
	</span>
	</div>
	<ul>
		<li>
			You are about to upload a brickle file.
		</li>
	</ul>
	<ul><li></li></ul>
	<ul>
		<li class="has right">
			<span>
			<a class="button grey" data-project-sure><i class="box-loop"></i>Replace</a>
			<span class="sure">
				<label>Are you sure?</label>
				<a class="button gray" data-upload="replace">Yes Replace</a>
				<a class="button gray" data-close-sure="">No</a>
			</span>
			</span>
			<a class="button grey" data-upload="merge"><i class="box-list-add"></i>Merge</a>
			<a class="button" data-upload="new"><i class="box-doc-new"></i>New Project</a>
		</li>
	</ul>
</div>