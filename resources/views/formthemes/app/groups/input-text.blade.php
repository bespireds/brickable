<div class="row form-group{{ $errors->has($name) ? ' has-error' : '' }}">
	<?php $fid= str_slug('fld-'.$label.'-'.$name); ?>
	<label class="small-4 columns text-right" for="{{ $fid }}">{{ $label }}</label>
	<div class="w66 columns end">
		<input type="text" class="form-control" name="{{ $name }}" value="{{ old($name) }}" id="{{ $fid }}" >
		@if ($errors->has($name))
			<span class="help-block">
				{{ $errors->first($name) }}
			</span>
		@endif
	</div>
</div>