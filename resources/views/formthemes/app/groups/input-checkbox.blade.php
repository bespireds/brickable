<div class="row form-group">
	<?php $fid= str_slug('fld-'.$label.'-'.$name); ?>
	<div class="w66 small-offset-4 columns">
		<div class="checkbox">
			<label>
				<input type="hidden" name="{{ $name }}" value="0">
				<input type="checkbox" name="{{ $name }}" id="{{ $fid }}"> <span for="{{ $fid }}">{{ $label }}</span>
			</label>
		</div>
	</div>
</div>