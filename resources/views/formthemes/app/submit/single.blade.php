<div class="form-group row">
	<div class="small-6 small-offset-4 columns">
		<button type="submit" class="btn btn-primary">
			<i class="fa fa-btn fa-{{ $icon }}"></i> {{ $label }}
		</button>
	</div>
</div>