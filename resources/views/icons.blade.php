@extends('layouts.wires')

@section('content')

<style>
	.one-icon-box{ float:left; width:{{ $maxlen * 12 }}px; padding-bottom:8px; }
	.one-icon-box i{ margin-right: 4px; }
	.one-icon-box span{ font-size:14px; }
	.all-the-icons{ padding: 1em; font-size: 20px; font-family:monospace; }
	
</style>

	<div class="all-the-icons">
	@foreach($icons as $icon => $number)
	<div class="one-icon-box">
		<i class="{{ $icon }}"></i>
		<span>{{ $number }} {{ $icon }}</span>
	</div>
	@endforeach
	</div>
@endsection

