<form data-model-id="{{ $model->id }}">
<input type="hidden" name="project_id" value="0">
<input type="hidden" name="type" value="model">
<div class="header">
	<span>
		<input type="text" name="name" placeholder="{{ $model->name or 'New model name' }}" value="{{ $model->name }}">
	</span>
	<span class="right">
		<a class="close-button right" data-close-modal ><i class="box-cancel"></i></a>
	</span>
	<span class="select right">
		<select name="module">
		@foreach( $modules as $module_key => $module)
			<option 
				{{ $model->module == $module->key ? 'selected="selected"' : ''}}
				value="{{ $module->key }}" >{{ $module->label }}
			</option>
		@endforeach
		</select>
	</span>
</div>
<?php $checked= 'checked="checked"'; ?>
<ul>
	<li class="id_select">
		<span class="name">
			<input type="hidden"   name="has_id" value="">
			<input type="checkbox" name="has_id" id="f-id" 
			value="inc" {{ $model->index_type == 'inc' ? $checked : '' }}>
		</span>
		<span>
			<label for="f-id">Id</label>
		</span>
	</li>
</ul>
<ul class="columns">
@foreach( $entries as $entry )
	@include( 'modal.model.row', compact('entry', 'models') )
@endforeach
</ul>
<ul>
	<li class="has">
		<a class="" data-add-modelrow><i class="box-plus-boxed"></i>Add</a>
	</li>
</ul>
<ul>
	<li class="has">
		<span class="name">
			<input type="hidden"   name="has_created_at" value="0">
			<input type="checkbox" name="has_created_at" id="f-create" 
			value="1" {{ $model->has_created_at ? $checked : ''}}></span>
		<span><label for="f-create">Created at</label></span>
	
		<span class="name">
			<input type="hidden"   name="has_updated_at" value="0">
			<input type="checkbox" name="has_updated_at" id="f-update" 
			value="1" {{ $model->has_updated_at ? $checked : ''}}></span>
		<span><label for="f-update">Updated at</label></span>

		<span class="name">
			<input type="hidden"   name="has_deleted_at" value="0">
			<input type="checkbox" name="has_deleted_at" id="f-delete" 
			value="1" {{ $model->has_deleted_at ? $checked : ''}}></span>
		<span><label for="f-delete">Deleted at</label></span>
	</li>
</ul>

<ul>
	<li class="has right">
		<span>
		<a class="button grey" data-sure><i class="trash-box"></i>Delete</a>
		<span class="sure">
			<span>Are you sure?</span>
			<a class="button red" data-model-delete="{{ $model->id }}">Yes Delete</a>
			<a class="button gray" data-not-sure></i>Dont delete</a>
		</span>
		</span>
		<a class="button grey" data-close-modal><i class="box-cancel"></i>Cancel</a>
		<a class="button" data-save-model><i class="box-ok"></i>Save</a>
	</li>
</ul>
</form>
<ul class="blueprint">
	@include( 'modal.model.row', ['entry' => $blueprint, 'models' => $models] )
</ul>
<script>
	$('#editModal .columns').sortable({ axis: 'y' });
	$('#editModal ul').disableSelection();
	$('#editModal').keypress( function(e) {
		if(e.which == 13) {
			$(document.activeElement).blur();
		}
	});
	$('[name="project_id"]').val( $('#models').attr('data-project-id'));
</script>
