	<li class="{{ $entry->group or 'constant' }}">
		<span class="name"><i class="lines-box"></i>
			<input type="hidden" name="column_id[]" value="{{ $entry->id }}">
		</span>
		<span class="name">
			<input type="text" name="column_name[]" value="{{ $entry->name }}" placeholder="Name">
		</span>
		<span class="value">
			<input type="text" name="column_value[]" value="{{ $entry->value }}" placeholder="Value">
		</span>
		<span class="groups">
			<select name="column_group[]">
			@foreach( Config::get('columns.groups') as $group_key => $group_label)
				<option 
					{{ $entry->group == $group_key ? 'selected="selected"' : ''}}
					value="{{ $group_key }}" >{{ $group_label }}
				</option>
			@endforeach
			</select>
		</span>
		<span class="constants">
			<select name="column_constant[]">
			@foreach( Config::get('columns.constants') as $contstant_key => $contstant_label)
				<option 
					{{ $entry->type == $contstant_key ? 'selected="selected"' : ''}}
					value="{{ $contstant_key }}">{{ $contstant_label }}
				</option>
			@endforeach
			</select>
		</span>
		<span class="dates">
			<select name="column_date[]">
			@foreach( Config::get('columns.dates') as $date_key => $date_label)
				<option 
					{{ $entry->type == $date_key ? 'selected="selected"' : ''}}
					value="{{ $date_key }}">{{ $date_label }}
				</option>
			@endforeach
			</select>
		</span>
		<span class="relations">
			<select name="column_relation[]">
			@foreach( Config::get('columns.relations') as $relation_key => $relation_label)
				<option 
					{{ $entry->type == $relation_key ? 'selected="selected"' : ''}}
					value="{{ $relation_key }}">{{ $relation_label }}
				</option>
			@endforeach
			</select>
		</span>
		<span class="models">
			<select name="column_model[]">
			@foreach( $models as $selectable)
				<option 
					{{ ($entry->relation_id == $selectable->id) ? 'selected="selected"' : ''}}
					value="{{ $selectable->id }}">{{ $selectable->name }}
				</option>
			@endforeach
			</select>
		</span>
		<span class="actions">
			<i class="delete-box"></i>
		</span>
	</li>