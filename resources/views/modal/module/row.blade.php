	<li class="module">
		<span class="label"><i class="lines-box"></i>
			<input type="hidden" name="module_id[]" value="{{ $module->id or '' }}">
		</span>
		<span class="readonly">
			<input type="text" name="module_key[]" value="{{ $module->key or '' }}" placeholder="key" readonly="true">
		</span>
		<span class="name">
			<input type="text" name="module_label[]" value="{{ $module->label or '' }}" data-abide-ignore placeholder="label">
		</span>
		<span class="actions">
			<i class="delete-box"></i>
		</span>
	</li>