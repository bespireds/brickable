<form>
<div class="header">
	<span>
		Modules for project {{ $project->name }}
	</span>
	<span class="right">
	<a class="close-button right" data-close-modal ><i class="box-cancel"></i></a>
	</span>	
</div>
<?php $checked= 'checked="checked"'; ?>
<ul class="columns">
@foreach( $modules as $module )
	@include( 'modal.module.row', compact('modules') )
@endforeach
</ul>
<ul>
	<li class="has">
		<a class="" data-add-modelrow><i class="box-plus-boxed"></i>Add</a>
	</li>
</ul>
<ul>
	<li class="has right">
		<a class="button grey" data-close-modal><i class="box-cancel"></i>Cancel</a>
		<a class="button" data-save-module><i class="box-ok"></i>Save</a>
	</li>
</ul>
</form>
<ul class="blueprint">
	@include( 'modal.module.row', ['module' => null] )
</ul>
<script>
	$('#editModal .columns').sortable();
//	$('#editModal ul').disableSelection();
	$('#editModal').keypress( function(e) {
		if(e.which == 13) {
			$(document.activeElement).blur();
		}
	});
</script>
