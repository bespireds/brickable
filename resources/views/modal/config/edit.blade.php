<form data-model-id="{{ $model->id }}">
<input type="hidden" name="project_id" value="0">
<input type="hidden" name="type" value="config">
<div class="header blue">
	<span>
		<input type="text" name="name" placeholder="{{ $model->name }}" value="{{ $model->name }}">
	</span>
	<span class="right">
		<a class="close-button right" data-close-modal ><i class="box-cancel"></i></a>
	</span>
	<span class="select right">
		<select name="module">
		@foreach( $modules as $module_key => $module)
			<option 
				{{ $model->module == $module->key ? 'selected="selected"' : ''}}
				value="{{ $module->key }}" >{{ $module->label }}
			</option>
		@endforeach
		</select>
	</span>
</div>
<?php $checked= 'checked="checked"'; ?>

<ul class="columns">
@foreach( $entries as $entry )
	@include( 'modal.config.row', compact('entry', 'models') )
@endforeach
</ul>
<ul>
	<li class="has">
		<a class="" data-add-modelrow><i class="box-plus-boxed"></i>Add</a>
	</li>
</ul>
<ul>
	<li class="has right">
		<a class="button grey" data-close-modal><i class="box-cancel"></i>Cancel</a>
		<a class="button" data-save-config><i class="box-ok"></i>Save</a>
	</li>
</ul>
</form>
<ul class="blueprint">
	@include( 'modal.config.row', ['entry' => $blueprint, 'models' => $models] )
</ul>

<script>
	$('#editModal .columns').sortable({ axis: 'y' });
	$('#editModal ul').disableSelection();
	$('#editModal').keypress( function(e) {
		if(e.which == 13) {
			$(document.activeElement).blur();
		}
	});
	$('[name="project_id"]').val( $('#models').attr('data-project-id'));
</script>
