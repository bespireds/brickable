	<li class="configrow">
		<span class="name"><i class="lines-box"></i>
			<input type="hidden" name="column_id[]" value="{{ $entry->id or '' }}">
			<input type="hidden" name="column_group[]" value="config">
			<input type="hidden" name="column_config[]" value="text">

		</span>
		<span class="label">
			<input type="text" name="config_key[]" value="{{ $entry->name or '' }}" placeholder="key">
		</span>
		<span class="title">
			<input type="text" name="config_label[]" value="{{ $entry->title or '' }}" placeholder="label">
		</span>

		<span class="actions">
			<i class="delete-box"></i>
		</span>
	</li>