<form>
<input type="hidden" name="project_id" value="{{ $project->id }}">
<input type="hidden" name="type" value="model">
<div class="header">
	<span class="project">
		<input type="text" name="projectname" placeholder="{{ $project->name }}" value="{{ $project->name }}">
	</span>
	<span class="right">
		<a class="close-button right" data-close-modal><i class="box-cancel"></i></a>
	</span>
	</div>
	<ul class="project">
	@foreach($values as $option)
	@if ($option['type']=='checkbox')
		<li class="">
			<span class="name">
				<input type="hidden"   name="{{ $option['key'] }}" value="0">
				<input type="checkbox" name="{{ $option['key'] }}" value="1" id="id-{{ $option['key'] }}"
				{{ ($option['value']==1)?'checked="checked"':''}}>
			</span>
			<span class="name">
				<label for='id-{{ $option['key'] }}'>{{ str_spacecase($option['key']) }}</label>
			</span>
		</li>
	@endif	
	@if ($option['type']=='input')
		<li class="constant">
			<span class="name">
				<label>{{ str_spacecase($option['key']) }}</label>
			</span>
			<span class="name">
				<input class="white" name="{{ $option['key'] }}" value="{{ $option['value'] }}">
			</span>
		</li>
	@endif
	@endforeach
	</ul>
	<ul>
		<li class="has right">
			<a class="button grey" data-close-modal><i class="box-cancel"></i>Cancel</a>
			<a class="button" data-save-project><i class="box-ok"></i>Save</a>
		</li>
	</ul>
<ul>
</form>
