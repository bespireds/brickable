@extends('layouts.wires')

@section('content')
@include('menus.sidecontrols',['mainmenu'=>true])
<div class="row projects">
    <div class="w66 centered columns text-center header">PROJECTS</div>
</div>
<div class="row projects">
    <div class="w66 centered columns text-center">
    <ul >
    @foreach( $projects as $project )
        <li>
            <a href="/project/{{ $project->id }}">{{ ucwords($project->name) }}</a>
            <i class="trash-lid-box" data-delete="{{ $project->id }}"></i>
            <span class="sure">
            <label>Are you sure?</label>
                <a class="button purple" href="/project/delete/{{ $project->id }}">Yes Delete</a>
                <a class="button purple" data-close-sure="">Dont delete</a>
            </span>
        </li>
    @endforeach
        <li class="new">
            <div class="label">
            <i class="box-transport"></i>
            <a>New <strong>Project</strong></a>
            </div>
            <div class="namerow">
                <span class="valifail"><i class="box-displeased"></i></span>
                <input type="text" name="name" placeholder="name">
                <a class="button">Create</a>
            </div>
        </li>
    </ul>
  </div>
</div>
@include('menus.uploadmodal')
@endsection