Route::group(['middleware' => ['~middleware~']], function () {
	Route::get    ('~path~/~classname~', ['as' => '~route~.index', 'uses' => '~namespace~Controller@index]);
	Route::get    ('~path~/~classname~/create', ['as' => '~route~.create', 'uses' => '~namespace~Controller@create']);
	Route::post   ('~path~/~classname~', ['as' => '~route~.store', 'uses' => '~namespace~Controller@store']); 
	Route::get    ('~path~/~classname~/{~classname~s}', ['as' => '~route~.show', 'uses' => '~namespace~Controller@show']);
	Route::get    ('~path~/~classname~/{~classname~s}/edit', ['as' => '~route~.edit', 'uses' => '~namespace~Controller@edit']);
	Route::put    ('~path~/~classname~/{~classname~s}', ['as' => '~route~.update', 'uses' => '~namespace~Controller@update']);
	Route::delete ('~path~/~classname~/{~classname~s}', ['as' => '~route~.destroy', 'uses' => '~namespace~Controller@destroy']);
});