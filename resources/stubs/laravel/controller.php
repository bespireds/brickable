<?php

namespace App\Http\~namespace~;

use Illuminate\Http\Request;

use App\Http\Requests\~namespace~Request.php;
use App\Http\Controllers\Controller;

class ~Class~Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('~route~.index')->with('items', ~Class~::all() );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('~route~.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\~namespace~Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(~namespace~Request $request)
    {
        $~classname~ = ~Class~::find($id);
        return view('~route~.edit')->with('~classname~', $~classname~);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $~classname~ = ~Class~::find($id);
        return view('~route~.show')->with('~classname~', $~classname~);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $~classname~ = ~Class~::find($id);
        return view('~route~.edit')->with('~classname~', $~classname~);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(~namespace~Request $request, $id)
    {
        //
        $~classname~= ~Class~::find($id);
        $~classname~->update($request->all());    
        return redirect()->route('~route~.index'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
