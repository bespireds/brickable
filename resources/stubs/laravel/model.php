<?php

namespace App\Data\Models\~Path~;

use Illuminate\Database\Eloquent\Model;

class ~Class~ extends Model
{
	//
	protected $table = '~table~';

	//
	protected $fillable = [
	~fillable~
	];

	// ELOQUENT
	~eloquent~

}
