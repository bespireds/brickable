<?php
// *** === CONTROLLER HELPERS  === ***

function controller_make_index($mod, $var){	
	$view = $mod.'.'.$var.'.index';
	$class= ucfirst($var);
	return "\t\treturn view('$view')->with('items', $class::all() );";
}

function controller_make_create($mod, $var){
	$view = $mod.'.'.$var.'.create';
	return "\t\treturn view('$view');";
}

function controller_make_show($mod, $var){
	$class= ucfirst($var);
	$view = strtolower($mod.'.'.$var.'.show');
	return "\t\t\${$var} = $class::find(\$id);\n\t\treturn view('$view')->with('{$var}', \${$var});";
}

function controller_make_edit($mod, $var){
	$class= ucfirst($var);
	$view = strtolower($mod.'.'.$var.'.edit');
	return "\t\t\${$var} = $class::find(\$id);\n\t\treturn view('$view')->with('{$var}', \${$var});";
}

function controller_make_store($mod, $var){
	$path = $mod.'.'.$var.'.index';
	$class= ucfirst($var);
	$php = '';
	$php.= "\t\t// Please insert Validation \n";
    $php.= "\t\t$class::create(\$request->all());\n";
    $php.= "\t\treturn redirect()->route('{$path}');";
    return $php;
}

function controller_make_update($mod, $var){
	$path = $mod.'.'.$var.'.index';
	$class= ucfirst($var);
	$php = '';
	$php.= "\t\t\${$var}= {$class}::find(\$id);	\n";
	$php.= "\t\t// Please insert Validation \n";
//	$this->validate($request, [
//  	'title' => 'required|unique|max:255',
//      'body'  => 'required',
//   ]);
	$php.= "\t\t\${$var}->update(\$request->all());	\n";
	$php.= "\t\treturn redirect()->route('{$path}'); \n";
	$php.= "\n\t\t// consider: function update({$class}Request \$request, {$class} \${$var}) \n";
	$php.= "\t\t// this would need a {$class}Request and a corrected routing. \n";

	return $php;
}

function controller_make_destroy($mod, $var){
	return '';
}
