<?php

//*** === BRICKABLE DATA === ***//

function mkdirs()
{
	return [
		'app/Http/routes'      => '0777',
		'app/Data/Models'      => '0777',
		'app/Data/Configs'     => '0777',

		'app/Data/Models/App'       => '0777',
		'app/Http/Controllers/App'  => '0777',
		'app/Http/Requests/App'     => '0777',
	];
}

function configs()
{
	return [
/*	~~ Configs ~~ */
	];	
}

function models()
{
	return [
/*	~~ Models ~~ */
	];
}

function controllers()
{
	return [
/*	~~ Controllers ~~ For now same as Models */
	];
}

function views()
{
	return [
/*	~~ Views ~~ */
	];
}

function stubs()
{
	return [
/*	~~ Compressed views ~~ */
	];
}
