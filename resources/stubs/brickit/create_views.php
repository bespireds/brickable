<?php 
// create Views
if (!in_array('--skip-views',$argv)){
	foreach (views() as $colon => $table) {
		if ( strpos($colon,":") !== false ){
			make_view_path($colon);
			make_views($colon, $table);
		}	 
	}
}