<?php
// *** === FILE HELPERS  === ***

function moveApp($root, $file)
{
	if ( substr($file, 0, 4) == 'App/' )
	{
		$made = ltrim( str_replace('App/', '', $file) , '/');
		if ( file_exists( getcwd() . $root . $made ) )
		{
			rename (getcwd() . $root . $made, getcwd() . $root . ltrim($file, '/') );
		}
	}
}

function inject($filename, $spot, $inject)
{

	if (!file_exists($filename)) return;
	if ($inject == '') return;
	
	$file= file_get_contents($filename);
	if (strpos( $file, $inject ) !== false ) return; 

	$pos = 0;
	foreach ($spot as $find) {
		$pos= strpos($file, $find, $pos) + strlen($find);
	}
	if ( $pos < 0 ){
		echo "Cannot find injectionpoint in $filename\n";
		return;	
	} 
	$file = substr($file, 0, $pos) . "\n" . $inject . substr($file, $pos);
	$file = preg_replace("/([\\ ]{4})/", "\t", $file);
	file_put_contents($filename, $file);

}

function app_path( $path = '' )
{
	return getcwd() . '/app/' . ltrim($path, '/');	
}

function public_path( $path = '' )
{
	return getcwd() . '/public/' . ltrim($path, '/');	
}

function view_path( $path = '' )
{
	return getcwd() . '/resources/views/' . ltrim($path, '/');	
}

function migration_path( $file = '', $root = '/database/migrations/' )
{
	if ( substr($file, 0, 18) == 'Created Migration:') $file= rtrim(substr($file,19));
	if ( ($file != '' ) && (substr($file,-4) != '.php')) $file .= '.php';
	return getcwd() . $root . ltrim($file, '/');
}

function model_path( $file = '', $root = '/app/Data/Models/')
{
	if ( substr($file, 0, 14) == 'Created Model:') $file= rtrim(substr($file,15));
	if ( ($file != '' ) && (substr($file,-4) != '.php')) $file .= '.php';
	// Weird Laravel error where scaffolding can't take place in an App folder... Argh.
	moveApp($root, $file);
	return getcwd() . $root . ltrim($file, '/');
}



