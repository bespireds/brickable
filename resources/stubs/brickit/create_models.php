<?php 
// create Models
if (!in_array('--skip-models',$argv)){
	foreach (models() as $colon => $table) {

		if ( strpos($colon,":") !== false ) make_model_create($colon, $table);
		// create Model one-to-many relation
		if ( strpos($colon,"*") !== false ) make_model_relation($colon, $table);
	}
}