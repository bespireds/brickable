<?php
// create Requests
if (!in_array('--skip-requests',$argv)){
// 	for now with models
	foreach (models() as $colon => $table) {
		if ( strpos($colon,":") !== false ){
			$path = colon2model($colon);
			$request            = "php artisan make:request {$path}Request";
			echo "$request \n";
			$shell  = shell_exec ( $request );

			$root = '/app/Http/Requests/';
			moveApp($root, $path.'Request.php');

			$filename= getcwd() . $root.$path . 'Request.php';
			inject($filename, ['public function authorize','{'], "\t\treturn true;" );
			inject($filename, ['rules()','//'], request_fillables($table) );
		}
	}
} 