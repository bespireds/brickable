<?php
// *** === REQUEST HELPERS  === ***

function request_fillables($table)
{
	//'app:phase' => 'inc;txt=name;int=of_p_config,uns;foreign=of_p_config,ref=id,on=cnf_phaseconfig;timestamps;',
	$re = "/(txt)=([\\S]+?)(;|,)/";
	preg_match_all($re, $table, $matches);

	if ( !isset($matches) ) return '';

	$fill = "";
	foreach ($matches[2] as $key => $varname) 
	{
		$fill.= "\t\t\t'".$varname."' => ['required'],\n";
	}
	return $fill;
}