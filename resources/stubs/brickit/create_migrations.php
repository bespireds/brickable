<?php
// create Migrations
if (!in_array('--skip-migrations',$argv)){
	foreach (models() as $colon => $table) {
		if ( strpos($colon,":") !== false ) make_migration_create($colon, $table);
		if ( strpos($colon,"*") !== false ) make_migration_update($colon, $table);
	}
}