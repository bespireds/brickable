<?php
// *** === CONFIG HELPERS  === ***
function make_config($colon, $data)
{
	$re = "/([\\S]*):([\\S]*)/"; 
	preg_match($re, $colon, $matches);
	
	$path= getcwd() . '/config/' . $matches[1];
	$file= $matches[2].'.php';
	
	if (!file_exists( $path )) mkdir( $path );
	$filename = $path . '/' . $file;
	$filedata = config_file($data);

	file_put_contents($filename, $filedata);
	
	return;
}

function config_file($data)
{
	$stub =[];
	$stub[] = '<'.'?'.'php';
	$stub[] = '';
	$stub[] = 'return [';
	foreach ($data as $key => $value) {
		$stub[]= "    '$key' => \"$value\",";
	}
	$stub[]= '];';

	return join("\n", $stub);
}