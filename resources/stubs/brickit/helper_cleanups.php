<?php

function do_cleanup()
{
	// models
	deltree(getcwd().'/app/Data/');

	// controllers
	$root= getcwd().'/app/Http/Controllers/';
	foreach( scandir($root) as $name) {
		if ( substr($name,0,1)!='.') {
			$path= $root.$name;
			if ( is_dir($path) )
			{
				if (!in_array($name, ['Auth'])) {
					deltree($path);
				}
			}
		}
	}

	// requests
	$root= getcwd().'/app/Http/Requests/';
	foreach( scandir($root) as $name) {
		if ( substr($name,0,1)!='.') {
			$path= $root.$name;
			if ( is_dir($path) )
			{
				if (!in_array($name, ['Auth'])) {
					deltree($path);
				}
			}
		}
	}

	// routes
	$root= getcwd().'/app/Http/routes/';
	foreach( scandir($root) as $name) {
		if ( substr($name,0,1)!='.') unlink($root.$name);
	}
	
	// route file
	$filename = getcwd().'/app/Http/routes.php';
	$fd= file_get_contents($filename);
	$fd= "<?php\n\n" . substr($fd, strpos($fd, '/*'));
	file_put_contents($filename, $fd);

	// migration
	$root= getcwd().'/database/migrations/';
	foreach( scandir($root) as $name) {
		if ( substr($name,0,1)!='.') {
			if ( intval(substr($name,0,4)) > 2015 ) unlink($root.$name);
		}
	}

	// config
	$root= getcwd().'/config/';
	foreach( scandir($root) as $name) {
		if ( substr($name,0,1)!='.') {
			$path= $root.$name;
			if ( is_dir($path) )
			{
				if (!in_array($name, ['auth','errors','layouts','vendor'])) {
					deltree($path);
				}
			}
		}
	}

	// view
	$root= getcwd().'/resources/views/';
	foreach( scandir($root) as $name) {
		if ( substr($name,0,1)!='.') {
			$path= $root.$name;
			if ( is_dir($path) )
			{
				if (!in_array($name, ['auth','errors','layouts','vendor'])) {
					deltree($path);
				}
			}
		}
	}
}

function delTree($dir) { 
	if (!file_exists($dir)) return;
	$files = array_diff(scandir($dir), array('.','..')); 
		foreach ($files as $file) { 
			(is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
		} 
	return rmdir($dir); 
} 