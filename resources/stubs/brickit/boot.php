<?php


if (in_array('--help',$argv)){
	echo "options:\n--cleanup\n--skip-migrations\n--skip-models\n--skip-requests\n--skip-controllers\n--skip-paths\n\n";
	exit;
}

if (in_array('--cleanup',$argv)){
	do_cleanup();
	exit;
}

$re = "/()-([0-9]*?)-([0-9]*?).php|.php/"; 
$phpname = preg_replace($re, "", $argv[0]).'.php';
$newname= str_replace('.php', '', $phpname) . '-' . date("Ymd-His") . '.php';
rename( $argv[0], $newname );
