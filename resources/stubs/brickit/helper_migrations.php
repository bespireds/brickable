<?php
// *** === MIGRATE HELPERS  === ***

function make_migration_create($colon, $table)
{
	$name  = str_replace(":", "_", $colon);
	$path  = colon2model($colon);

	$migrate            = "php artisan make:migration create_$name";
	echo "$migrate \n";
	$shell = shell_exec ( $migrate );

	$filename = migration_path($shell);
	$migrations[$name] = $filename;
	inject($filename, ['up()','//'], schema_create($name, blue2columnize($table)) );
	inject($filename, ['down','//'], schema_drop($name) );
}

function make_migration_update($colon, $table)
{
	$name  = str_replace("*", "_", $colon);
	$path  = colon2model($colon);

	$migrate            = "php artisan make:migration update_$name";
	echo "$migrate \n";
	$shell = shell_exec ( $migrate );

	$filename = migration_path($shell);
	inject($filename, ['up()','//'], schema_update($name, blue2columnize($table)) );
//	inject($filename, ['down','//'], schema_dropkey($name) );
}