<?php
// *** === ROUTE HELPERS  === ***

function routefile($path, $file, $class)
{
	$item   = plural($file);

	$root= [];
	$root['index']  = "{$path}/{$file}";                $rest['index']  =  'get   ';  $meth['index']  = 'index';
	$root['create'] = "{$path}/{$file}/create";         $rest['create'] =  'get   ';  $meth['create'] = 'create';
	$root['store']  = "{$path}/{$file}";                $rest['store']  =  'post  ';  $meth['store']  = 'store';
	$root['show']   = "{$path}/{$file}/{{$item}}";      $rest['show']   =  'get   ';  $meth['show']   = 'show';
	$root['edit']   = "{$path}/{$file}/{{$item}}/edit"; $rest['edit']   =  'get   ';  $meth['edit']   = 'edit';
	$root['update'] = "{$path}/{$file}/{{$item}}";      $rest['update'] =  'put   ';  $meth['update'] = 'update';
	$root['delete'] = "{$path}/{$file}/{{$item}}";      $rest['delete'] =  'delete';  $meth['delete'] = 'destroy';

	$class = str_replace('/', '\\', $class);

	$l= 25;
	foreach ($root as $v) { $l = max( $l, strlen($v)); }
	// currently 1 guard: web
	$routes = "Route::group(['middleware' => ['web']], function () {\n";

	foreach ($root as $key => $value) {
		$restfull = $rest[$key];
		$method   = $meth[$key];
		$uses     = substr("'{$class}Controller@$method'            ", 0, strlen($class) + 20 );
		$aspath   = substr("'{$path}.{$file}.$method'      ", 0, strlen($path) + strlen($file) + 12 );
		$request  = substr("'" . $value . "'" . str_repeat(' ',$l) , 0, $l + 2 );

		$routes.= "\tRoute::$restfull ($request, ['as' => $aspath , 'uses' => $uses ]); \n";

	}
	$routes.= "});\n";

	return $routes;
}

function write_routefile($path, $class)
{
	$data= routefile($path, $class);
	$filename = $path . '.php';
	$filepath = app_path('Http/routes/');
	file_put_contents($filepath . $filename, $data );
	
	return $filename;
}
