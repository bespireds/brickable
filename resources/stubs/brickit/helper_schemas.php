<?php
// *** === SCHEMA HELPERS  === ***

function functionname($colom)
{
	$parts= explode('_', $colom);
	return $parts[1];
}

function plural($name)
{
	return rtrim($name, 's') . 's';
}


function schema_create($name, $table)
{
	$name  = plural($name);

	$schema  = "\t\tSchema::create('$name', function(Blueprint \$table){\n";
	foreach ($table as $column) {
		if ( key($column) != '' ) $schema .= "\t\t\t" . columnize($column) ."\n";
	}
	$schema .= "\t\t});\n";
	return $schema;
}

function schema_update($name, $table)
{
	$name  = plural($name);

	$schema  = "\t\tSchema::table('$name', function(Blueprint \$table){\n";
	foreach ($table as $column) {
		if ( key($column) != '' ) $schema .= "\t\t\t" . columnize($column) ."\n";
	}
	$schema .= "\t\t});\n";
	return $schema;
}

function schema_drop($name)
{
	$name = rtrim($name, 's') . 's';
	$schema  = "\t\tSchema::drop('$name');\n";
	return $schema;
}


function columnize($column) 
{
	if ( key($column) == '' ) return '';
	$define= '$table';
	foreach ($column as $type => $value) {
		if ( $type == 'ref' )  $type = 'references';
		if ( $type == 'uns' )  $type = 'unsigned';
		if ( $type == 'idx' )  $type = 'index';		
		if ( $type == 'inc' )  $type = 'increments';
		if ( $type == 'del' )  $type = 'onDelete';

		if ( $type == 'def' )  $type = 'default';

		if ( $type == 'chr' ) $value = str_replace(":", ",", $value);
		if ( $type == 'chr' )  $type = 'char';
		if ( $type == 'txt' )  $type = 'text';
		if ( $type == 'int' )  $type = 'integer';
		if ( $type == 'boo' )  $type = 'boolean';
		if ( $type == 'flo' )  $type = 'float';

		if ( $type == 'dat' )  $type = 'date';
		if ( $type == 'dti' )  $type = 'dateTime';
		if ( $type == 'tim' )  $type = 'time';

		if ( $type == 'on' )   $value = plural($value);

		if ( $value == '' ) $define.= '->' . $type . '()';
		if ( $value != '' ) $define.= '->' . $type . "('$value')";
	}
	$define.= ';';
	return $define;
}

// $name  = 'group_page';
// $blue  = inc; int=page_id,uns,idx; int=group_id,uns,idx; 
//			foreign=page_id,ref=id,on=pages,del=cascade; foreign=group_id,ref=id,on=pages,del=cascade;
//			timestamps;

// $table = [
// 	[ 'inc'=>'id', 'uns'=>'' ],
// 	[ 'int'=>'page_id',  'uns'=>'', 'idx'=>''],
// 	[ 'int'=>'group_id', 'uns'=>'', 'idx'=>''],
// 	[ 'foreign'=>'page_id',  'ref' =>'id', 'on' =>'pages',  'del'=>'cascade' ],
// 	[ 'foreign'=>'group_id', 'ref' =>'id', 'on' =>'groups', 'del'=>'cascade' ],
// 	[ 'timestamps' => '']
// ];
function blue2columnize($blue)
{
	$blue= str_replace('inc;', 'inc=id,uns;', $blue);

	$defines= [];
	foreach( explode(';', $blue) as $row ){
		$def= [];
		foreach(explode(',',$row) as $define){
			if (substr($define, 0, 8) != 'foreign:'){
				$pair= explode('=', $define);
				$def[trim($pair[0])]= (isset($pair[1])) ? trim($pair[1]) : '';
			}
		}
		$defines[]= $def;
	}	
	return $defines;
}