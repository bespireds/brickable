<?php 
// create Paths
if (!in_array('--skip-paths',$argv)) {
// 	for now with models
	foreach (models() as $colon => $path) {  
		if ( strpos($colon,":") !== false ){
			$re = "/([\\S]*):([\\S]*)/"; 
			preg_match($re, $colon, $matches);
			$path= $matches[1];
			$file= $matches[2];

			$class  = colon2model($colon);
			$to_inject= routefile($path, $file, $class);

			$filename = app_path("/Http/routes/{$path}.php");

			if (!file_exists($filename)) {
				$prepend = "<?"."php\n\n";		
			}else{
				$prepend= file_get_contents($filename);
			}
			file_put_contents($filename, $prepend.$to_inject);
			echo "path $filename \n";

			// Add route/routes.php to routes.php
			$string = "include __DIR__ . '/routes/{$path}.php';";
			$checkfile= file_get_contents(app_path('Http/routes.php'));	
			if ( strpos($checkfile, $string) === false )
			{
				inject( app_path('Http/routes.php'), ['<?'.'php', "\n"], $string );
			}
		}
	}
}