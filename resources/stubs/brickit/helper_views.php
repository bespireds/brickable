<?php
// *** === VIEW HELPERS === ***
function make_view_path($colon)
{
	$re = "/([\\S]*):([\\S]*)/"; 
	preg_match($re, $colon, $matches);
	$path= $matches[1];
	$file= $matches[2];

	if (!file_exists(view_path($path))) mkdir(view_path($path));
	if (!file_exists(view_path($path.'/'.$file))) mkdir(view_path($path.'/'.$file));

}

function make_views($colon, $table){
	preg_match("/([\\S]*):([\\S]*)/", $colon, $matches);
	$title= ucwords($matches[2]);
	$item = $matches[2];
	$route= str_replace(':', '.', $colon);
	$path= view_path($matches[1].'/'.$matches[2].'/');

	$re = "/(text|date)=([\\S]*?);/"; 
	preg_match_all($re, $table, $fields);
	
	$stubs= create_stubs();
	
	// index
	$file= str_replace( 
		['~title~','~row~','~route~'], 
		[$title, make_index_rows($colon, $table), $route.'.create'],
		$stubs['form_index']);
	file_put_contents($path.'index.blade.php', $file);

	// create
	$file= str_replace( 
		[ '~title~','~formfields~','~routename~'],		
		[ $title, make_input_rows($table, 'create'), $route.'.store'], 
		$stubs['form_create']);
	file_put_contents($path.'create.blade.php', $file);

	//edit
	$file= str_replace( 
		['~title~','~formfields~','~routename~','~route~','~id~'],
		[$title, make_input_rows($table, 'input', $item ), $route.'.update', $route.'.index', $item.'->id'], 
		$stubs['form_edit']);
	file_put_contents($path.'edit.blade.php', $file);

	//show
	$file= str_replace( 
		['~title~','~formfields~'], 
		[$title, make_input_rows($table, 'show', $item)], 
		$stubs['form_show']);
	file_put_contents($path.'show.blade.php', $file);

}

function make_index_rows($colon, $table)
{
	$stubs= create_stubs();
	
	$re = "/([\\S]*):([\\S]*)/"; 
	preg_match($re, $colon, $matches);
	$path= $matches[1];
	$file= $matches[2];

	$re = "/(text|date)=([\\S]*?);/"; 
	preg_match_all($re, $table, $fields);

	$second= $fields[2][1];
	$first=  $fields[2][0];
	if (!isset($first))  $first= 'null';
	if (!isset($second)) $second= 'null';

	$input= $stubs['row_index'];
	$input= str_replace('~mod~', $path, $input);
	$input= str_replace('~var~', $file, $input);

	$input= str_replace('~first~',  $first, $input);
	$input= str_replace('~second~', $second, $input);
	
	return $input;
}

function make_input_rows($table, $type = 'input', $itemname = '')
{
	$stubs= create_stubs();
	
	$re = "/(text|date)=([\\S]*?);/"; 
	preg_match_all($re, $table, $fields);

	$formfields= [];
	foreach ($fields[2] as $key => $fieldname) {
//		$field= $fields[1][$key];
//		$input= $stubs[$type.'_'.$fields];

		$input= $stubs[$type.'_text'];

		$input= str_replace('~title~', $fieldname, $input);
		$input= str_replace('~name~',  $fieldname, $input);
		$input= str_replace('~item~',  $itemname,  $input);

		$formfields[]= $input;
	}
	return join("\n", $formfields);
}