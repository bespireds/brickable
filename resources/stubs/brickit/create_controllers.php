<?php 
// create Controllers
if (!in_array('--skip-controllers',$argv)){
	foreach (models() as $colon => $path) { // for now with models

		if ( strpos($colon,":") !== false ){
			$name  = str_replace(":", "_", $colon);
			$path  = colon2model($colon);
			
			$controller          = "php artisan make:controller {$path}Controller --resource";
			echo "$controller \n";
			$shell  = shell_exec ( $controller );

			$root = '/app/Http/Controllers/';
			moveApp($root, $path.'Controller.php');

			$filename = getcwd() . $root . $path . 'Controller.php';
			preg_match("/([\\S]*):([\\S]*)/", $colon, $matches);
			echo "adjust controller: ".$filename ."\n";

			$class= str_replace('/', '\\', $path);
			$mod= $matches[1];
			$var= $matches[2];
			inject($filename, ['Requests','Controller;'], "use App\\Data\\Models\\{$class};\n" );
			inject($filename, ['function index(',   '//'], controller_make_index($mod, $var)   );
			inject($filename, ['function create(',  '//'], controller_make_create($mod, $var)  );
			inject($filename, ['function edit(',    '//'], controller_make_edit($mod, $var)    );
			inject($filename, ['function show(',    '//'], controller_make_show($mod, $var)    );
			inject($filename, ['function store(',   '//'], controller_make_store($mod, $var)   );
			inject($filename, ['function update(',  '//'], controller_make_update($mod, $var)  );
// 			inject($filename, ['function destroy(', '//'], controller_make_destroy($matches[1], $matches[2]) );
		}
	}
} 