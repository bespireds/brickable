<?php
// *** === MODEL HELPERS  === ***
function make_model_create($colon, $table)
{
	global $models;
	$name  = str_replace(":", "_", $colon);
	$path  = colon2model($colon);
	
	$model              = "php artisan make:model Data/Models/$path";
	echo "$model \n";
	$shell      = shell_exec ( $model );

	$filename = model_path( $path );
	$models[$name] = $filename;

	inject($filename, ['Model','//'], model_fillables($table) );
	inject($filename, ['Model','//'], model_create($name) );

}

function make_model_relation($colon, $table)
{
	global $models;
	$name  = str_replace("*", "_", $colon);
	$fname = substr($colon, 1 + strpos($colon, '*'));
	$path  = colon2model($colon);
	// int=app_campaign_id,uns;foreign=app_campaign_id,ref=id,on=app_campaign;

	$re = "/on=([\\S]+?);/"; 
	preg_match($re, $table, $matches);
	$model = $matches[1];
	
	$re = "/foreign=([\\S]+?),/"; 
	preg_match($re, $table, $matches);
	$id = $matches[1];

	// has many in related model
	$filename = $models[$model];
	$relation = str_replace('/', '\\', 'App/Data/Models/'.$path );
	inject($filename, ['Model','ELOQUENT'], model_hasMany( $relation, plural($fname), $id));

	// belongs to in this model	
	$filename = $models[$name];
	$path  = colon2model($model);
	$relation = str_replace('/', '\\', 'App/Data/Models/'.$path );
	inject($filename, ['Model','ELOQUENT'], model_belongsTo( $relation, functionname($model) ));

}


function colon2model($name)
{
	$parts = explode(":", $name);
	if( count(explode("*", $name)) > count($parts) ) $parts= explode("*", $name);
	if( count(explode("_", $name)) > count($parts) ) $parts= explode("_", $name);

	foreach ($parts as $key => $part) {
		$parts[$key]= str_replace(" ", "", 
			ucwords(str_replace("_", " ", $part))
		);
	}
	$path  = join('/', $parts);
	return $path;
}


function model_create($name) 
{
	$name = plural($name);
	return "\tprotected \$table = '$name';\n\n\t//";
}


function model_fillables($table)
{
//'app:phase' => 'inc;txt=name;int=of_p_config,uns;foreign=of_p_config,ref=id,on=cnf_phaseconfig;timestamps;',
	$re = "/(txt|int|chr|boo)=([\\S]+?)(;|,)/";
	preg_match_all($re, $table, $matches);

	if ( !isset($matches) ) return '';

	$fill = "\tprotected \$fillable = [\n";
	foreach ($matches[2] as $varname) 
	{
		$fill.= "\t\t'".$varname."',\n";
	}
	$fill.= "\t];\n\n\t// ELOQUENT";
	return $fill;
}
// BelongsTo
// BelongsToMany
// HasMany
// HasManyThrough
// HasOne
// HasOneOrMany
// MorphMany
// MorphOne
// MorphOneOrMany
// MorphPivot
// MorphTo
// MorphToMany
// Pivot
// Relation

//	public function campaignmembers()
//	{
//		return $this->hasMany('App\Data\Models\App\Campaignmembers','app_campaign_id')->orderby('order');
//	}
function model_hasMany($path, $model, $table)
{
	$func = "\tpublic function $model()\n";
	$func.= "\t{\n";
	$func.= "\t\treturn \$this->hasMany('$path','$table');\n";
	$func.= "\t}\n";
	return $func;	
}

function model_belongsTo($path, $model)
{
	$func = "\tpublic function $model()\n";
	$func.= "\t{\n";
	$func.= "\t\treturn \$this->belongsTo('$path');\n";
	$func.= "\t}\n";
	return $func;	
}