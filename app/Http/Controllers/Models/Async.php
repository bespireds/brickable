<?php

namespace App\Http\Controllers\Models;

use App\Data\GetSetters\ColumnsTrait;
use App\Data\Models\Columns;
use App\Data\Models\Models;
use App\Data\Models\ModelsValue;
use App\Data\Models\Modules;
use App\Data\Models\Projects;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

class Async extends Controller
{
	use ColumnsTrait;
	
// JSON

	public function show($id)
	{
		$model= Models::find($id);
		$model->getValues();
		$combi =[
			'model'   => $model,
			'columns' => Models::find($id)->columns()->get()
		];
		return response()->json($combi);
	}

// VIEWS

	public function config($id)
	{
		$project= Projects::find($id);

		return view('modal.config.edit')
			->with('blueprint' , new Columns())
			->with('model'     , new Models())
			->with('models'    , $project->models()->get())
			->with('modules'   , $project->modules()->get())
			->with('entries'   , [ new Columns() ]);
	}

	public function create($id)
	{
		$project= Projects::find($id);

		return view('modal.model.edit')
			->with('blueprint' , new Columns())
			->with('model'     , new Models())
			->with('models'    , $project->models()->get())
			->with('modules'   , $project->modules()->get())
			->with('entries'   , [ new Columns() ]);
	}

	public function edit($id)
	{
		$model= Models::find($id);
		$model->getValues();

		return view("modal.{$model->type}.edit")
			->with('blueprint' , new Columns())
			->with('modules'   , $model->project->modules()->get())
			->with('models'    , $model->project->models()->get())
			->with('model'     , $model)
			->with('entries'   , $model->columns()->orderby('order')->get());

	}
	
// RETURN

	// new or update
	public function update(Request $request, $id)
	{
		$new_id= self::store($request, $id);
		return response()->json(['id' => $new_id]);

	}
	
	public function action(Request $request, $id, $action, $var1, $var2)
	{
		switch($action) 
		{
			case "position":
				ModelsValue::set($id, 'screen_left', $var1 );
				ModelsValue::set($id, 'screen_top',  $var2 );
			break;
		}	
	}

	public function delete($id)
	{
		Models::find($id)->delete();
	}

}
