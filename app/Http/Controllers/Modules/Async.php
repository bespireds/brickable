<?php

namespace App\Http\Controllers\Modules;

use App\Data\Models\Modules;
use App\Data\Models\Projects;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

class Async extends Controller
{

// VIEWS

	public function edit($id)
	{
		$project= Projects::find($id);

		return view('modal.module.edit')
			->with('project', $project)
			->with('modules', $project->modules()->get());

	}
	
// RETURN

	// new or update
	public function update(Request $request, $id)
	{
		$order  = 1;
		$project= Projects::find($id);
		$modules= $project->modules()->get();

		$module_ids    =  $request->get('module_id');
  		$module_keys   =  $request->get('module_key');
  		$module_labels =  $request->get('module_label');

		$module_ids = array_map('intval', $module_ids);
		$deletes= array_diff(
				$modules->lists('id')->toArray(), 
				$module_ids);
		
		Modules::destroy($deletes);
		
  		foreach ($module_labels as $key => $dummy) {

  			$id= $module_ids[$key];
  			
  			if ( $modules->contains('id', $id ) )
  			{

  				$module= Modules::find($id);
  				$module->update([
  					'order' => $key,
  					'key'   => $module_keys[$key],
					'label' => Modules::fixLabel($module_labels[$key])
				]);

  			}else{
  			
  				$mkey= $module_keys[$key];
				$mlab= Modules::fixLabel($module_labels[$key]);

				if ( $mkey = Modules::fixkey($project->id, $mkey, $mlab) )
				{
					$module= Modules::create([
						'project_id' => $project->id,
						'order'      => $key,
						'key'        => $mkey,
						'label'      => $mlab
					]);
  				}
  			}
  		}

		return response()->json(['done' => '']);
	}


}
