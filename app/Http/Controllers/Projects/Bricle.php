<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller;
use App\Http\Providers\BrickitFormat;
use App\Http\Providers\BricleFormat;
use Illuminate\Http\Request;

/*
projects:
	- my first project

models:
	- (1) Marketing-group(s)
	- (2) Company(s)	
	- (3) MarketingGroup-Member(s)
	- (4) User(s)
	- (5) CampaignMember(s)
	- (6) Campaign(s)
	- (7) Task(s)
	- (8) Row(s)
	- (9) Description(s)

models_values:
	- :1, integer, screen_left, 80
	- :1, integer, screen_left, 80

columns:
	- :1, name, constant, text, 
	- :1, name, constant, one-to-one, :2

*/

class Bricle extends Controller
{
	

	public function import(Request $request)
	{
		
		$bricle = new BricleFormat();
		$project= $bricle->import($request);

		return response()->json($project->id);

	}

	
	public function export($id)
	{
		
		$bricle = new BricleFormat();
		$filepath = $bricle->export($id);		

		return response()->download($filepath);
		
	}

	
	public function download($id)
	{
		
		$bricle = new BrickitFormat();
		$filepath = $bricle->export($id);		

		return response()->download($filepath);
		
	}




}
