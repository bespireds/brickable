<?php

namespace App\Http\Controllers\Projects;

use App\Data\Models\Models;
use App\Data\Models\Modules;
use App\Data\Models\Projects;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class View extends Controller
{

	public function index()
	{
		return view('welcome')->with('projects', Projects::all() );
	}

	public function show($id)
	{
		$project = Projects::find($id);
		return view('schema')->with('project', $project);

	}

	public function icons()
	{		
		$file = File::get(base_path('resources/assets/sass/brickable/components/fontello.scss'));
		$re = "/\\n.([1-9a-z\\-]*?):before[\\S\\s]*?\\'([\\S]*?)'/"; 
		preg_match_all($re, $file, $matches);
		$icons=[];
		$maxlen= 5;
		foreach ($matches[1] as $key => $name) {
			$icons[$name]=$matches[2][$key];
			$maxlen = max($maxlen, 5+strlen($name));
		}
		return view('icons')->with('icons', $icons)->with('maxlen', $maxlen);
	}

	public function delete($id)
	{
		$project = Projects::find($id);
		if ( $project ) $project->delete();
		
		return redirect('/');
	}

	public function make($name)
	{
		$name = ucwords(str_replace('-', ' ', $name));
 		$project = Projects::create(['name' => $name]);


 		$project->modules()->createMany(config('project.modules'));
		$project->values()->createMany(config('project.values'));

 		return redirect('/project/'.$project->id);

	}

}
