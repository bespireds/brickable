<?php

namespace App\Http\Controllers\Projects;

use App\Data\Models\Models;
use App\Data\Models\Projects;
use App\Data\Models\ProjectsValue;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Async extends Controller
{

// VIEWS

	public function edit($id)
	{
		$project= Projects::find($id);

		return view('modal.project.edit')
			->with('project', $project)
			->with('values', $project->values()->get());
	
	}



	public function ajax($id)
	{

		$models= Projects::find($id)->models()->get();
		foreach ($models as $model) {
			$model->getValues();
		}

		$combi =[
			'models'  => $models,
			'columns' => Projects::find($id)->columns()->get()
		];

		return response()->json($combi);
	}

	public function rename($id, $name)
	{
		$name = trim(urldecode($name));
		if ($name == '') $name = 'No Name';

		Projects::find($id)->update(['name' => $name ]);

		return response()->json([ 'name' => $name ]);
	}

	public function update(Request $request, $id)
	{
		$projectvalues= Projects::find($id)->values()->get()->toArray();

		
		foreach ($projectvalues as $value) {
			if ( $request->has($value['key']) )
			{
				if ( $request->get($value['key']) != $value['value'])
				{
					ProjectsValue::find($value['id'])
						->update(['value' => $request->get($value['key'])]);
				}
			}
		}
		return response()->json(['id' => $id]);

	}

}
