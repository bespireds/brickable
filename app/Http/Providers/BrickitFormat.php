<?php

namespace App\Http\Providers;

use App\Data\Models\Columns;
use App\Data\Models\Models;
use App\Data\Models\ModelsValue;
use App\Data\Models\Projects;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;

Class BrickitFormat {

	protected static $convert= [
		'text'      => 'txt',
		'char'      => 'chr',
		'integer'   => 'int',
		'boolean'   => 'boo',
		'float'     => 'flo',

		'date-time' => 'dti',
		'date'      => 'dat',
		'time'      => 'tim',			 
	];
	

	public function brickit_file($name) {

		// glue all parts.
		$files= [
			'boot.php',
			'create_folders.php',
			'create_configs.php',
			'create_controllers.php',
			'create_requests.php',
			'create_migrations.php',
			'create_models.php',
			'create_paths.php',
			'create_views.php',
			'exit.php',
			'helper_auth.php',
			'helper_cleanups.php',
			'helper_configs.php',
			'helper_controllers.php',
			'helper_files.php',
			'helper_migrations.php',
			'helper_models.php',
			'helper_requests.php',
			'helper_routes.php',
			'helper_schemas.php',
			'helper_views.php',
			'helper_stubs.php',
			'data.php',
		];
		

		$brickit = "<?php\n";
		foreach ($files as $key => $file) {
			$content= File::get(base_path('resources/stubs/brickit/'.$file));
			// $re = "/(\\n)[\\s]*?\\/\\/[\\s\\S]*?\\n/"; 
			// $content = preg_replace($re, "\n\n", $content);			
			// $content = preg_replace($re, "\n", $content);

			// $re = "/(\\n)([\\t])*/"; 
			// $content = preg_replace($re, "\n", $content);

			$brickit.= str_replace("<?php", "", $content);
		}
		
		File::put(base_path('resources/views/brickit/'.str_slug($name).'.php'),$brickit );

	}

	public function export($id) {
	   
	   	$project= Projects::find($id);
	   	$this->brickit_file($project->name);

	   	$filename= 'brickit.'.str_slug($project->name);

		$view = View::make($filename);
		$filedata = File::get($view->getPath());

		$project= Projects::find($id);
		$models = $project->models;
		
		$data= self::collectModels($models);
		$filedata= self::inject($filedata, '~~ Models ~~', $data);

		$data= self::collectViews($models);
		$filedata= self::inject($filedata, '~~ Views ~~', $data); // Temp also in views.

		$data= self::collectConfigs($models);
		$filedata= self::inject($filedata, '~~ Configs ~~', $data);

		$data= self::createStubs();
		$filedata= self::inject($filedata, '~~ Compressed views ~~', $data);

		$filename = str_slug($project->name) . '.php';
		$filepath = storage_path('app/brl/' . $filename);

		if (File::exists($filepath)) File::delete($filepath);
		File::put($filepath, $filedata);

		return $filepath;
	}


	private static function collectConfigs($models)
	{
		$data= [];
		foreach ($models as $model) 
		{
			if ( $model->type == 'config')
			{
				$model->getValues();

				$viewname  = self::tablename($model->module);
				$tablename = self::tablename($model->name);
				
				$pairs= [];
				foreach ($model->columns as $column)
				{
					$key   = $column['name'];
					$value = $column['title'];
					
					$pairs[] = "'$key'=>\"$value\"";
				}
				$array= join( ', ', $pairs );

				$data[]= "\t\t'$viewname:$tablename' => [ $array ],";
			}
		}

		return $data;
	}

	private static function collectViews($models)
	{
		$data= [];
		foreach ($models as $key => $model) {
			if ( $model->type == 'model')
			{
				$model->getValues();
				
				$viewname  = self::tablename($model->module);
				$tablename = self::tablename($model->name);
				$shorthand = '';
				foreach ($model->columns as $column)
				{
					$group= $column->group . 'View';
					$shorthand.= self::$group($column);
				}
				$data[]= "\t\t'$viewname:$tablename' => '$shorthand',";
			}
		}
		return $data;
	}


	private static function collectModels($models)
	{
		$data= [];

		foreach ($models as $model) {
			$model->depth = self::childrenlevels($model, 0);
		}
		
		$models = $models->sortBy('depth');
		
		foreach ($models as $model) 
		{
			if ( $model->type == 'model')
			{
				$model->getValues();

				$viewname  = self::tablename($model->module);
				$tablename = self::tablename($model->name);

				$shorthand = "";
				$shorthand.= "inc;";

				foreach ($model->columns as $column)
				{
					$column->parent= $model;
					$group= $column->group;
					$shorthand.= self::$group($column);
				}

				$shorthand.= "timestamps;";

				$data[]= "\t\t'$viewname:$tablename' => '$shorthand',";
			}
		}
		
		// append foreign keys
		foreach ($models as $model) 
		{
			foreach ($model->columns as $column)
			{
				$type= $column->type;
				if ( $type == 'one-to-many'){
					
					$model->getValues();					
					$tablename = self::tablename($model->name);
					$viewname  = self::tablename($model->module);

					$modelname = $viewname . '_' . $tablename;
					$id_name  = $modelname . '_id';

					$shorthand= 'int='.$id_name.',uns;foreign='.$id_name.',ref=id,on='.$modelname.';';
					
					$relation = Models::find($column->relation_id);
					$relation->getValues();	
					$tablename = self::tablename($relation->name);
					$viewname  = self::tablename($relation->module);

					$code= "\t\t'$viewname*$tablename' => '$shorthand',";
					if ( !in_array($code, $data))
					{
						$data[]= $code;
					}

				}
			}
		}

		return $data;
	}

  
	private static function createStubs()
	{
		$data = self::createStubParts(base_path('resources/stubs/laravel'), '.php');
		$data.= self::createStubParts(base_path('resources/stubs/view'), '.stub');
		return $data;
	}

	private static function createStubParts($path, $ext)
	{
		$data = '';
		$files= scandir($path);
		$len  = strlen($ext);
		foreach ($files as $file) {
			if (substr($file, -$len) == $ext)
			{
				$stub= str_replace('.','_',substr($file, 0, -$len));
				$stubdata = file_get_contents($path.'/'.$file);
				$data .= substr("\t'$stub'                   ",0, 23);
				$data .= "=> '". rtrim(strtr(base64_encode(gzdeflate($stubdata, 9)), '+/', '[]'), '=');
				$data .= "',\n";
			}
		}
		return $data;
	}

	private static function constantView($column)
	{
		$name= str_replace([' ','-'], '_', trim($column->name));
		return $column->type . '=' . $name . ';';
	}
	private static function dateView($column)
	{
		$name= str_replace([' ','-'], '_', trim($column->name));
		return $column->type . '=' . $name . ';';
	}
	private static function relationView($column)
	{
		return '';
	}

	private static function constant($column)
	{
		$type= self::$convert[$column->type];
		$name= str_replace([' ','-'], '_', trim($column->name));
		if ( $type == 'boo' ) return $type . '=' . $name . ',def=0;';
		return $type . '=' . $name . ';';

	}
	
	private static function date($column)
	{
		$type= self::$convert[$column->type];
		$name= str_replace([' ','-'], '_', trim($column->name));
		return $type . '=' . $name . ';';
	}

	private static function relation($column)
	{

		if ( $column->type == 'parent-id'  )   return self::parent_id($column);
		if ( $column->type == 'one-to-one'  )  return self::one_to_one($column);
		if ( $column->type == 'one-to-many' )  return '';
		if ( $column->type == 'many-to-many' ) return '';
		
		return '';
	}

	private static function parent_id($column)
	{
		$name= str_replace([' ','-'], '_', trim($column->name));

		$model= $column->parent;

		$space   = self::tablename($model->module);		
		$relname = self::tablename($model->name);

		$relname = $space . "_" . $relname;

		$shorthand  = 'int='.$name.',uns;';
		$shorthand .= 'foreign='.$name.',ref=id,on='.$relname.';';

		return $shorthand;
	}

	private static function one_to_one($column)
	{
		$name= str_replace([' ','-'], '_', trim($column->name));

		$relation = Models::find($column->relation_id);
		$relation->getValues();

		$space   = self::tablename($relation->module);		
		$relname = self::tablename($relation->name);

		$relname = $space . "_" . $relname;

		$shorthand  = 'int='.$name.',uns;';
		$shorthand .= 'foreign='.$name.',ref=id,on='.$relname.';';

		return $shorthand;
	}

	private static function tablename($name)
	{

		$noplural = str_replace( '(s)', '', $name );
		$nodash   = str_replace( '-', '', $noplural );
		$tolower  = strtolower( $nodash );

		return $tolower;

	}


	private static function inject($haystack, $needle, $inject)
	{
		if ( gettype($inject) == 'array' ) $inject= join("\n",$inject);
		$pos= strpos($haystack, $needle);
		if ( $pos === false  ) return $haystack;

		$pos= strpos($haystack, "\n", $pos);
		return substr($haystack, 0, $pos) . "\n" . $inject . substr($haystack, $pos);

	}


	private static function childrenlevels( $model, $level )
	{

		$children = $model->columns->reject(function ($column) {
			return $column->group != 'relation';
		});		

		if ( count($children) == 0 ) return $level;

		$max_level = $level;
		foreach ($children as $key => $child) {

			$model = Models::find($child->relation_id);
		
			$new_level= self::childrenlevels($model, $level+1);
		
			if ($new_level > $max_level) $max_level= $new_level;
		
		}

		return $max_level;

	}



}
