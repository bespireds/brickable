<?php

namespace App\Http\Providers;

use App\Data\Models\Columns;
use App\Data\Models\Models;
use App\Data\Models\ModelsValue;
use App\Data\Models\Modules;
use App\Data\Models\Projects;
use App\Data\Models\ProjectsValue;
use Illuminate\Support\Facades\File;

Class BricleFormat {

	protected static $format = "project:\n{project}{pvalues}\nmodules:\n{modules}\nmodels:\n{models}\nvalues:\n{values}\ncolumns:\n{columns}\n";
	protected static $project = "\t- {name}\n";
	protected static $p_value = "\t- {type}~ {key}~ {value}\n";
	protected static $module  = "\t- ({key}) {label}\n";
	protected static $model   = "\t- ({idx}) {type}~ {name}\n";
	protected static $value   = "\t- :{id}~ {type}~ {key}~ {value}\n";
	protected static $column  = "\t- :{id}~ {name}~ {group}~ {type}~ {relation_id}~ {title}\n";
	
	protected static $module_types=[
		'm' => 'model', 
		'c' => 'config',
	];
	protected static $project_value_types=[
		'c' => 'checkbox', 
		'i' => 'input',
	];
	protected $modulelist = [];
	protected $modellist  = [];
	protected $brcl = '';
	protected $project_id = 1;


	public function import($request) {

		$brcl = $this->getBrcl($request);
		if ($brcl == '')
			return;

		$this->project_id = $request->get('project_id');
		$uploadtype = $request->input('uploadtype');

		$project = $this->getProject($uploadtype);

		$this->fillProject($project);
		$this->fillModels($project);
		$this->fillModules($project);
		$this->fillColumns();
		$this->fillValues($project);

		return $project;
	}

	public function export($id) {

		$project = Projects::find($id);
		$project_str = str_replace('{name}', $project->name, self::$project);

		$pvalues= $project->values()->get();
		$pvaluelist = $pvalues->map(function ($item, $idx) {
				return str_replace(
					['{type}', '{key}', '{value}'], 
					[substr($item->type, 0, 1), $item->key, $item->value], 
					self::$p_value);
			})->toArray();

		$modules = Modules::whereProjectId($id)->get();
		$modulelist = $modules->map(function ($item, $idx) {
				return str_replace(['{key}', '{label}'], [$item->key, $item->label], self::$module);
			})->toArray();

		
		$models = Models::whereProjectId($id)->get();
		$modellist = $models->map(function ($item, $idx) {
				return str_replace(
					['{idx}', '{type}', '{name}'], 
					[$idx + 1, substr($item->type, 0, 1), $item->name], 
					self::$model);
			})->toArray();

		$reverse   = self::getReverse($models);
		$valuelist = self::getValues($models);
		
		$columnlist = self::getColumns($models, $reverse);
		
		$text = self::$format;
		$text = str_replace('{project}', $project_str, $text);
		$text = str_replace('{pvalues}', join($pvaluelist), $text);
		$text = str_replace('{modules}', join($modulelist),  $text);
		$text = str_replace('{models}',  join($modellist),   $text);
		$text = str_replace('{values}',  join($valuelist),   $text);
		$text = str_replace('{columns}', join($columnlist),  $text);

		$filename = str_slug(Projects::find($id)->name) . '.brl';
		$filepath = storage_path('app/brl/' . $filename);

		if (File::exists($filepath))
			File::delete($filepath);

		File::put($filepath, $text);

		return $filepath;
	}


	private function getReverse($models){
		$reverse= [];
		foreach ($models as $mdx => $model) {
			$reverse[$model->id] = $mdx + 1;
		}
		return $reverse;
	}

	private function getValues($models){
		$valuelist= [];
		foreach ($models as $mdx => $model) {
			$values = ModelsValue::whereModelId($model->id)->get();
			$list = $values->map(function ($item) use ($mdx) {
					return str_replace(
						['{id}', '{type}', '{key}', '{value}'], [$mdx + 1, $item->type, $item->key, $item->value], self::$value
					);
				})->toArray();
			$valuelist = array_merge($valuelist, $list);
		}
		return $valuelist;
	}

	private function getColumns($models, $reverse){
		$columnlist= [];
		foreach ($models as $mdx => $model) {
			$values = Columns::whereModelId($model->id)->orderBy('order')->get();
			$list = $values->map(function ($item) use ($mdx, $reverse) {
					$link = 'null';
					if ($item->relation_id) {
						$link = ':' . $reverse[$item->relation_id];
					}
					return str_replace(
						['{id}', '{name}', '{group}', '{type}', '{relation_id}', '{title}'], 
						[$mdx + 1, $item->name, $item->group, $item->type, $link, $item->title], self::$column
					);
				})->toArray();
			$columnlist = array_merge($columnlist, $list);
		}
		return $columnlist;
	}


	private function getProject($uploadtype) {

		switch ($uploadtype) {
			case 'new':
				$project = Projects::create(['name' => $this->fileProjectName()]);
				break;

			case 'replace':
				$project = Projects::find($this->project_id);
				$project->unlink();
				$project->update(['name' => $this->fileProjectName()]);
				break;

			case 'merge':
				$project = Projects::find($this->project_id);
				break;
		}

		return $project;
	}

	private function fileProjectName() {
		$projects = self::part($this->brcl, "/project:([\\s\\S]+?)\\n\\n|$/");
		return reset($projects);
	}

	private function fillModels($project) {
		$models = self::part($this->brcl, "/models:([\\s\\S]+?)\\n\\n|$/");

		$modellist = [];
		foreach ($models as $model_val) {
			$re = "/\\(([\\d]*)\\) ([\\S])+~ ([\\S]*)/";  
			preg_match($re, $model_val, $parts);

			$idx = trim( $parts[1] );
			$modellist[$idx] = Models::firstOrCreate([
					'project_id' => $project->id,
					'type' => self::mtype( trim($parts[2]) ),
					'name' => trim( $parts[3] )
			]);
		}
		$this->modellist = $modellist;


	}

	private static function mtype($type){
		$module_types = self::$module_types;
		if ( !array_key_exists($type, $module_types) ) return 'model';
		return $module_types[$type];
	}

	private function fillModules($project) {
		$modulelist = self::part($this->brcl, "/modules:([\\s\\S]+?)\\n\\n|$/");

		foreach ($modulelist as $module_val) {
			$re = "/\\) ([\\S]*)/";
			preg_match($re, $module_val, $labelarr);

			$re = "/\\(([\\S]*)\\)/";
			preg_match($re, $module_val, $keyarr);

			if (!$keyarr)
				dd('No module index found in: ', $models);

			$idx = trim($keyarr[1]);
			$modulelist[$idx] = Modules::firstOrCreate([
				'project_id' => $project->id,
				'key'        => trim($keyarr[1]),
				'label'      => trim($labelarr[1])
			]);
		}
		$this->modulelist = $modulelist;
	}

	private function fillColumns() {
		$columns = self::part($this->brcl, "/columns:([\\s\\S]+?)\\n\\n|$/");

		$modellist = $this->modellist;

		foreach ($columns as $column_key => $column_val) {

			$parts = explode('~', $column_val); // :1, name, constant, text, null

			$id  = substr(trim($parts[0]), 1);
			$rel = trim($parts[4]); 

			$link = null;
			if (isset($rel) && ( substr($rel, 0, 1) == ':' )) {
				$ptr = substr($rel, 1);
				if (isset($modellist[$ptr])) {
					$link = $modellist[$ptr]->id;
				}
			}
			
			$re = "/\\:([0-9]*)/";
			preg_match_all($re, $column_val, $idxarr);

			$column = Columns::firstOrCreate([
					'model_id' => $modellist[$id]->id,
					'name'     => trim($parts[1]),
					'group'    => trim($parts[2]),
					'type'     => trim($parts[3]),
					'title'    => trim($parts[5]),
			]);

			$column->update([
				'order'        => $column_key,
				'relation_id'  => $link
			]);
		}
	   
	}
	private function fillProject($project){
		$values = self::part($this->brcl, "/project:([\\s\\S]+?)\\n\\n|$/");
		foreach ($values as $value_val) {
			$parts = explode('~', $value_val); // :1, integer, screen_left, 80
			if ( count($parts) == 3)
			{
				$type = trim($parts[0]);
				$key  = trim($parts[1]);
				$value= trim($parts[2]);

				$pvalue = ProjectsValue::firstOrCreate([
					'project_id' => $project->id,
					'type'       => self::$project_value_types[$type],
					'key'        => $key,
				]);
				$pvalue->update([
					'value' => $value
				]);
			}
		}
	}

	private function fillValues($project) {
		$values = self::part($this->brcl, "/values:([\\s\\S]+?)\\n\\n|$/");

		$modellist = $this->modellist;
		foreach ($values as $value_val) {
			$parts = explode('~', $value_val); // :1, integer, screen_left, 80

			$re = "/\\:([0-9]*)/";
			preg_match_all($re, $value_val, $idxarr);

			$idx = $idxarr[1][0];
			if (!isset($parts[3])) $parts[3] = '';

			$type = trim($parts[1]);
			$key  = trim($parts[2]);
			$value= trim($parts[3]);

			if ( $type == 'integer' ) $value= round($value);

			$modelvalue = ModelsValue::firstOrCreate([
					'model_id' => $modellist[$idx]->id,
					'type'     => $type,
					'key'      => $key
				]);
			$modelvalue->update([
				'value' => $value
				]);
		}
	}

	private function getBrcl($request) {
		$brcl = '';
		$filepath = storage_path('app/brl/');
		$files = $request->file('files');
		foreach ($files as $key => $file) {

			if (File::exists($filepath))
				File::delete($filepath);

			$request->file("files.{$key}")->move($filepath, 'uploaded.brl');
			$filecontents = File::get($filepath . '/uploaded.brl');
			if (strpos($filecontents, 'project:') !== false) {
				$brcl = $filecontents;
			}

			File::delete($filepath . '/uploaded.brl');
		}

		$this->brcl = $brcl;

		return $brcl;
	}

	private function part($brcl, $re) {
		preg_match($re, $brcl, $filter);
		if ($filter[0] == '') return [];

		$filters = explode("\t- ", $filter[1]);

		
		foreach ($filters as $key => $entry) {
			$filters[$key] = trim($entry);
			if ($filters[$key] == '')
				unset($filters[$key]);
		}
		return $filters;
	}

}
