<?php

Route::get ('/model/async/{id}/{action}/{var1}/{var2}', 'Models\Async@action');

Route::get ('/project/async/rename/{id}/{name}', 'Projects\Async@rename');
Route::get ('/project/async/{id}',        'Projects\Async@ajax');
Route::any ('/project/async/{id}/edit',   'Projects\Async@edit');
Route::any ('/project/async/{id}/update', 'Projects\Async@update');

Route::get ('/project/delete/{id}',       'Projects\View@delete');
Route::get ('/project/make/{name}',       'Projects\View@make');

Route::get ('/project/brickit/{id}',	 'Projects\Bricle@download');
Route::get ('/project/export/{id}',	     'Projects\Bricle@export');
Route::post('/project/async/import',     'Projects\Bricle@import');

Route::get ('/module/async/{id}/edit',   'Modules\Async@edit');
Route::post('/module/async/{id}/update', 'Modules\Async@update');

Route::get ('/model/async/{id}/edit',    'Models\Async@edit');
Route::get ('/model/async/{id}/delete',  'Models\Async@delete');

Route::any ('/model/async/{id}/update',  'Models\Async@update');
Route::get ('/model/async/{id}/show',    'Models\Async@show');
Route::get ('/model/async/{id}/create',  'Models\Async@create');
Route::get ('/model/async/{id}/config',  'Models\Async@config');


Route::get ('/icons',  'Projects\View@icons');
