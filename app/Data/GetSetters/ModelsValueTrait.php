<?php

namespace App\Data\GetSetters;

use App\Data\Models\Models;
use App\Data\Models\ModelsValue;



trait ModelsValueTrait
{
	
	static public function set($id, $key, $value)
	{
		$entry = ModelsValue::where('model_id', $id)->whereKey($key)->first();

		if ( !$entry ){
			if (isset(Models::$values[$key]))
			{
				$entry = ModelsValue::create([
					'model_id' => $id,
					'type'     => Models::$values[$key],
					'key'      => $key,
					'value'    => $value
				]);
			}
			return;
		}
		$entry->update(['value' => $value]);
	}

	static public function get($id, $key)
	{
		$var= self::whereModelId($id)->whereKey($key)->first();
		if ( $var ) return $var->$key;
		return;
	}

}
