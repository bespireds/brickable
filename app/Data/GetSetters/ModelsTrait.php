<?php

namespace App\Data\GetSetters;

use App\Data\Models\ModelsValue;

trait ModelsTrait
{
	

	public function valuesUpdate($values)
	{
		foreach ($values as $key => $value) {
			ModelsValue::set($this->id, $key, $value );
		}
	}

	public function getValues()
	{	
		$values = $this->values()->get();
		foreach ($values as $vkey => $value) {
			$key = $value->key;
			$this->$key = $value->value;
		}		
	}

	public function setValues()
	{	
		foreach (self::$values as $key) 
		{
			ModelsValue::set($this->id, $key, $this->$key );
		}
	}

}
