<?php

namespace App\Data\GetSetters;

use App\Data\Models\Columns;
use App\Data\Models\Models;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

trait NamesTrait 
{

	public static function shortName($name)
	{

		if ( strpos($name, '-') > 0 )
		{
			$parts = explode('-', $name);
			$last = array_pop($parts);
			foreach ($parts as &$part) {
				$capitals = preg_replace("/([^A-Z])/", '', $part);
				if (strlen($capitals) == 0) $capitals = substr(ucfirst($part),0,1);
				$part= $capitals;
			}
			return join('-', $parts) .'-'. $last;
		}

		return $name;
	}

	private function modelName($name)
	{
		$name     = rtrim($name, '(s)');
		$name     = str_replace(['(',')'], '', $name);
		$plural   = str_plural($name);
		$name     = str_singular($plural);
		if ($name == '') $name = 'noname';
		$name = ucfirst($name);
		return $name . '(s)';
	}
}
