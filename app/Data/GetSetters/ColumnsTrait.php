<?php

namespace App\Data\GetSetters;

use App\Data\Models\Columns;
use App\Data\Models\Models;
use App\Data\Models\Projects;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

trait ColumnsTrait 
{

	public function store(Request $request, $id)
	{

		if ( intval($id) == 0 )
		{
			$project_id = $request->get('project_id');
			$model= Models::create(compact('project_id')+['type'=>'config']);
			
			$id= $model->id;
			$model->valuesUpdate([
				'screen_left' => 500 + floor(rand(-40,40)),
				'screen_top'  => 80  + floor(rand(-20,20))
			]);
		}

		$model     = Models::find($id);
		
		$project   = Projects::find($model->project_id);
		$models    = $project->models->toArray();

		$modelname = self::modelName( $request->name, $models );
		$modeltype = $request->input("type");

		$update = $request->only(['name', 'type', 'module', 'has_created_at', 'has_updated_at', 'has_deleted_at']);
		$update['index_type'] = $request->input('has_id');
		
		$model->valuesUpdate( $update );
		$model->update([ 'name' => $modelname , 'type' => $modeltype ]);
	
		
		$column_ids= $request->get('column_id');

		Columns::whereModelId($id)->update(['order' => -1]);
		
		$model_id = $model->id;
			
		if ( count($column_ids) > 0 )
		{
			foreach ($column_ids as $key => $value) 
			{

				$id          = (int)$request->input("column_id.$key");
				$order       = (int)$key + 1;
				$relation_id = null;

				switch($modeltype)
				{
					case 'config':
						$name = preg_replace("/[^A-Za-z0-9]/", '', $request->input("config_key.$key"));
						$title = $request->input("config_label.$key");
						$group = 'config';
						$type  = 'text';
						if ($name =='') $name= "idx{$key}";

						break;
						
					default:
						$relation_id = (int)$request->input("column_model.$key");
						$name  = $request->input("column_name.$key");
						$group = $request->input("column_group.$key");
						$type  = $request->input("column_$group.$key");
						if ($name =='') $name= 'noname';
				}

				if ($group=='relation') 
				{
					if ( $relation_id ) $relation= Models::find($relation_id);
					if ($type=='one-to-one'){
						$shortname = self::shortName($relation->name);
						$relation_name= strtolower(str_replace('(s)', '', $shortname));
						$name = 'of_' . str_singular($relation_name);
					}
					if ($type=='one-to-many'){
						$shortname = self::shortName($relation->name);
						$relation_name= strtolower(str_replace('(s)', '', $shortname));
						$name = 'has_' . str_plural($relation_name);
					}
					if ($type=='parent-id'){
						$shortname = self::shortName($relation->name);
						$relation_name= strtolower(str_replace('(s)', '', $shortname));
						$name = str_singular($relation_name) . '_id';
					} 
				}

				$update = compact('order','name','group','type','relation_id','model_id','title');
				$column = Columns::updateOrCreate(['id' => $id], $update );

			}
		}
		Columns::where('order', -1 )->delete();
	
		return $model_id;
	}

	public static function shortName($name)
	{

		if ( strpos($name, '-') > 0 )
		{
			$parts = explode('-', $name);
			$last = array_pop($parts);
			foreach ($parts as &$part) {
				$capitals = preg_replace("/([^A-Z])/", '', $part);
				if (strlen($capitals) == 0) $capitals = substr(ucfirst($part),0,1);
				$part= $capitals;
			}
			return join('-', $parts) .'-'. $last;
		}

		return $name;
	}

	private function modelName($name, $model)
	{
		$name     = rtrim($name, '(s)');
		$name     = str_replace(['(',')'], '', $name);
		$plural   = str_plural($name);
		$name     = str_singular($plural);

		if (($name == '') && ($model)) $name = $this->newName($model);
		$name = ucfirst($name);

		return $name . '(s)';
	}

	private function newName($models)
	{
		$names= [ 'noname', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth', 'eleventh', 'twelfth', 'thirteenth' ];
		$name = array_shift($names);
		while ( $this->nameInArray( $this->modelName($name,null), $models )){
			$name = array_shift($names);
		}
		return $name;
	}

	private function nameInArray($search, $array)
	{
		$foundObject = array_filter(
			$array,
			function ($e) use (&$search) {
				return $e['name'] == $search;
			}
		);
		return count($foundObject) > 0;
	}


}
