<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Data\GetSetters\ModelsValueTrait;

class ModelsValue extends Model
{
	use ModelsValueTrait;

	protected $dates = ['deleted_at'];
	
	protected $table = 'models_values';

	protected $fillable = [
		'model_id',
		'type',
		'key',
		'value'
	];

	public function model()
	{
		return $this->belongsTo('App\Data\Models\Columns');
	}


}
