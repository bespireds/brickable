<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Columns extends Model
{
	//
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];
	
	protected $table = 'columns';

	protected $fillable = [
		'order',
		'name',
		'group',
		'type',
		'title',
		'relation_id',
		'model_id',
	];

	public function models()
	{
		return $this->belongsTo('App\Data\Models\Models')->orderBy('order');
	}

}
