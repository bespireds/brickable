<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Data\GetSetters\ModelsTrait;

class Models extends Model
{
	use ModelsTrait;
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];
	
	protected $table = 'models';

	protected $fillable = [
		'name',
		'type',
		'project_id',
	];

	public static $values = [
		'index_type'     => 'string',
		'module'         => 'string',
		'has_created_at' => 'boolean',
		'has_updated_at' => 'boolean',
		'has_deleted_at' => 'boolean',
		'screen_left'    => 'integer',
		'screen_top'     => 'integer',
	];

	
// ELOQUENT

	public static function boot()
	{
		parent::boot();    

		static::deleted(function($model)
		{
			foreach($model->columns as $column)
			{
				$column->delete();
			}
			foreach($model->values as $value)
			{
				$value->delete();
			}
		});
	}

	
	public function project()
	{
		return $this->belongsTo('App\Data\Models\Projects');
	}

	public function columns()
	{
		return $this->hasMany('App\Data\Models\Columns','model_id')->orderby('order');
	}

	public function values()
	{
		return $this->hasMany('App\Data\Models\ModelsValue','model_id');
	}


}
