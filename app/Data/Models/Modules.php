<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modules extends Model
{
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];
	
	protected $table = 'modules';

	protected $fillable = [
		'key',
		'label',
		'order',
		'project_id',
	];

	
// ELOQUENT
	
	public function project()
	{
		return $this->belongsTo('App\Data\Models\Projects', 'project_id');
	}


	static function fixlabel($mlab)
	{
		return str_singular($mlab);
	}

	static function fixkey($prj, $mkey, $mlab)
	{
		if ( strlen( trim( $mkey ) ) > 0 ) return $mkey; 
		
		$mkey = strtolower(trim($mlab));
		$re   = "/[aeiouy]/i"; 
		$chrs = str_split(preg_replace($re, '', $mkey));
		if (count($chrs)<3) $chrs = str_split($mkey);
		$e= count($chrs)-1; $m= floor($e / 2);

		$mkey = $chrs[0] . $chrs[$m] . $chrs[$e];

		return $mkey;

	}


}
