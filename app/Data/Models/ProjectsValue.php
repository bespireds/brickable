<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectsValue extends Model
{

	protected $dates = ['deleted_at'];
	
	protected $table = 'projects_values';

	protected $fillable = [
		'project_id',
		'type',
		'key',
		'value'
	];

	public function project()
	{
		return $this->belongsTo('App\Data\Models\Projects');
	}


}
