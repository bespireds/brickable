<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Projects extends Model
{
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];
	
	protected $table = 'projects';

	protected $fillable = [
		'name',
	];

	public static function boot()
	{
		parent::boot();    

		static::deleted(function($project)
		{
			$project->unlink();
		});
	}

	public function unlink()
	{
		foreach($this->models as $model)
		{
			$model->delete();
		}
	}


	public function modules()
	{
		return $this->hasMany('App\Data\Models\Modules', 'project_id')->orderBy('order');
	}

	public function models()
	{
		return $this->hasMany('App\Data\Models\Models', 'project_id');
	}

	public function columns()
	{
		return $this->hasManyThrough('App\Data\Models\Columns', 'App\Data\Models\Models', 'project_id', 'model_id')->orderBy('order');
	}

	public function values()
	{
		return $this->hasMany('App\Data\Models\ProjectsValue', 'project_id');
	}


}
