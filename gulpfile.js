process.env.DISABLE_NOTIFIER = true;
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
	
	mix.sass('brickle.scss');
	mix.scripts([

		'stubs.js',
		'wireboxes.js',
		'models2html.js',    	

		'ui_gui.js',
		'ui_project.js',
		'ui_config.js',
		'ui_module.js',
		'ui_model.js',

	], 'public/js/brickle.js');
});
