<?php

use App\Data\Models\Columns;
use App\Data\Models\Models;
use App\Data\Models\ModelsValue;
use App\Data\Models\Projects;
use Illuminate\Database\Seeder;

class ModelSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	protected static $models = [
		[ 1, 'Marketing-group(s)',   80,  40, 'inc', 1,1,0 ],
		[ 2, 'Company(s)',          360,  40, 'inc', 1,1,0 ],
		[ 3, 'Member(s)',           360, 160, 'inc', 1,1,0 ],
		[ 4, 'Contact(s)',          680,  40, 'inc', 1,1,0 ],
	];

	protected static $columns = [
		[ 2, 1, 'name',       'constant', 'text', null],
		[ 3, 1, 'of_company', 'relation', 'one-to-one',  2],
		[ 4, 1, 'member',     'relation', 'one-to-many', 3],
		[ 6, 2, 'name',       'constant', 'text', null],
		[ 8, 3, 'name',       'constant', 'text', null],
		[ 9, 3, 'contact',    'relation', 'one-to-one',  4],
		[10, 4, 'name',       'id',       'inc',  null],
		[11, 4, 'name',       'constant', 'text', null],
		[12, 2, 'contact',    'relation', 'one-to-one',  4],
	];
	// .id , .constant , .date , .relation , .config , .enum , .dynum  

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		DB::table('projects')->truncate();
		DB::table('models')->truncate();
		DB::table('columns')->truncate();        
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');


		$project= Projects::create(['name' => 'my first project']);

		foreach (self::$models as $model) {
			$parent= Models::create([
				'project_id'     => $project->id,
				'name'           => $model[1],
			]);
			ModelsValue::create([ 'model_id' => $parent->id, 'type' => 'integer', 'key' => 'screen_left',    'value' => $model[2] ]);
			ModelsValue::create([ 'model_id' => $parent->id, 'type' => 'integer', 'key' => 'screen_top',     'value' => $model[3] ]);
			ModelsValue::create([ 'model_id' => $parent->id, 'type' => 'string',  'key' => 'index_type',     'value' => $model[4] ]);
			ModelsValue::create([ 'model_id' => $parent->id, 'type' => 'boolean', 'key' => 'has_created_at', 'value' => $model[5] ]);
			ModelsValue::create([ 'model_id' => $parent->id, 'type' => 'boolean', 'key' => 'has_updated_at', 'value' => $model[6] ]);
			ModelsValue::create([ 'model_id' => $parent->id, 'type' => 'boolean', 'key' => 'has_deleted_at', 'value' => $model[7] ]);
		};

		$idx= 1;
		foreach (self::$columns as $column) {
			Columns::create([
				'model_id'      => $column[1],
				'order'         => $idx++,
				'name'          => $column[2],
				'group'         => $column[3],
				'type'          => $column[4],
				'relation_id'   => $column[5]
			]);
		};  
	}
}
