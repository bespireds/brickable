<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumns extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('columns', function(Blueprint $table){
			$table->increments('id')->unsigned();			
			$table->integer('order');
			$table->char('group', 16);
			$table->char('type',  16);
			$table->char('name',  32);
			$table->string('title');
			$table->integer('relation_id')->nullable()->default(null);
			$table->integer('model_id')->nullable()->unsigned()->index()->default(null);
			$table->timestamps();
			$table->softDeletes();
			$table->foreign('model_id')->references('id')->on('models')->onDelete('cascade');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('columns');

	}
}
