<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTables extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function(Blueprint $table){
			$table->increments('id')->unsigned();			
			$table->string('name');
			$table->timestamps();
			$table->softDeletes();
		});
	
		Schema::create('projects_values', function(Blueprint $table){
			$table->increments('id')->unsigned();
			$table->integer('project_id')->unsigned();
			$table->char('type',   16)->nullable();
			$table->char('key',    64)->nullable();
			$table->char('value', 128)->nullable();
			$table->timestamps();
			$table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');

		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('projects_values');
		Schema::drop('projects');

	}
}
