<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelTables extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('models', function(Blueprint $table){
			$table->increments('id')->unsigned();
			$table->integer('project_id')->unsigned();
			$table->string('name');
			$table->string('type', 32)->default('model');
			$table->timestamps();
			$table->softDeletes();
			$table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
		});

		Schema::create('models_values', function(Blueprint $table){
			$table->increments('id')->unsigned();
			$table->integer('model_id')->unsigned();
			$table->char('type',  16)->nullable();
			$table->char('key',   64)->nullable();
			$table->char('value', 64)->nullable();
			$table->timestamps();
			$table->foreign('model_id')->references('id')->on('models')->onDelete('cascade');
		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('models_values');
		Schema::drop('models');

	}
}
