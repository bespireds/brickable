<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleTables extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('modules', function(Blueprint $table){
			$table->increments('id')->unsigned();
			$table->integer('project_id')->unsigned();
			$table->integer('order');
			$table->string('key');
			$table->string('label');
			$table->timestamps();
			$table->softDeletes();
			$table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('modules');

	}
}
