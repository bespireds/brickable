function getStub(what)
{
	var html = '';
	switch(what)
	{
		case 'modal-model':
			html += '<div class="header">Loading...<a class="close-button right" data-close-modal="" aria-label="Close"><i class="icon-cancel"></i></a></div>';
			html += '<ul><li class="has right"><span><a class="button grey" data-model-sure=""><i class="icon-trash"></i>Delete</a></span><a class="button grey" data-close-modal=""><i class="icon-cancel"></i>Cancel</a><a href="" class="button"><i class="icon-ok"></i>Save</a></li></ul>';
		break;
		case 'modal-module':
			html += '<div class="header">Loading...<a class="close-button right" data-close-modal="" aria-label="Close"><i class="icon-cancel"></i></a></div>';
			html += '<ul><li class="has right"><span><a class="button grey" data-project-sure=""><i class="icon-trash"></i>Delete</a></span><a class="button grey" data-close-modal=""><i class="icon-cancel"></i>Cancel</a><a href="" class="button"><i class="icon-ok"></i>Save</a></li></ul>';
		break;
		case 'modal-config':
			html += '<div class="header">Loading...<a class="close-button right" data-close-modal="" aria-label="Close"><i class="icon-cancel"></i></a></div>';
			html += '<ul><li class="has right"><span><a class="button grey" data-model-sure=""><i class="icon-trash"></i>Delete</a></span><a class="button grey" data-close-modal=""><i class="icon-cancel"></i>Cancel</a><a href="" class="button"><i class="icon-ok"></i>Save</a></li></ul>';
		break;

		case 'html-field':
			html = '<li class="[typecol]" [connect]><span>[name]</span><span class="[narrow]">[reltype]</span></li>';
		break;

		case 'html-model':
			html+= '<div class="model [type]" ';
			html+= '    data-id="[id]" data-name="[mname]" ';
			html+= '    style="top:[y]px; left: [x]px; z-index: [zidx]">';
			html+= '    <div class="header">[name]<a href="" data-model-edit="[id]" class="right"><i class="view-edit-box"></i></a></div>';
			html+= '    <ul class="body"></ul>';
			html+= '</div>';
		break;
		case 'update-header':
			html+= '    [name]<a href="" data-model-edit="[id]" class="right"><i class="view-edit-box"></i></a>';
		break;		
	}
	return html;

}
function wired()
{

	$('[data-connect]').each(function(){ 

		var mynameis  = $(this).attr('data-connect');
		var modelname = '[data-name="#"]'.replace('#',mynameis);
		if ($(modelname).length > 0 )
		{
			t1= $(modelname).offset().top + 16;
			l1= $(modelname).offset().left;
			h1= $(modelname).height();

			t2= $(this).offset().top  + 10;
			l2= $(this).offset().left + $(this).width();
			w2= $(modelname).width();
	
			var id= '#' + $(this).attr('data-wire');


			wires(id, Math.floor(t1),Math.floor(l1),Math.floor(t2),Math.floor(l2), h1,w2);
		}

	});

}

function wires(id, t1,l1,t2,l2, h1, w2)
{

	if (( l1 - l2 > -w2 ) && ( l1- l2 < 20 )) {

		if ( t2 > t1 ){

			top_mid_wires(id, t1,l1,t2,l2, h1);

		}else{

			bottom_mid_wires(id, t1,l1,t2,l2);

		}

		return;
	
	}

	if ( l2 < l1 ){

		if ( t2 > t1 ){

			top_right_wires(id, t1,l1,t2,l2);

		}else{
		
			bottom_right_wires(id, t1,l1,t2,l2);
	
		}
	}else{
		if ( t2 > t1 ){

			top_left_wires(id, t1,l1,t2,l2);

		}else{

			bottom_left_wires(id, t1,l1,t2,l2);
			
		}
	}
}

function top_mid_wires(id, t1,l1,t2,l2, drop)
{
	l1 -= 40; l2 += 40;
	var w= l2-l1+20, h= t2-t1+8; drop -= 5;
	var ex= w-2, ey= h-7, eym= ey-40, exm= ex-40, eyd= drop+(ey-drop)/2;
	
	var exd= ex-40 ;
	$(id + ' svg').attr('width', w+'px').attr('height', h+'px').css({left: l1, top: t1});
	var path= 'M 40 5 C 0 5  0 ~drop~ 40 ~drop~ ';
	var path= path + 'L ~exd~ ~drop~ ';

	var path= path + 'C ~exd~ ~drop~ ~ex~ ~drop~ ~ex~ ~eyd~ ';
	var path= path + 'C ~ex~ ~eyd~ ~ex~ ~ey~ ~exm~ ~ey~ ';

	path = path.replace(/~drop~/g, drop).replace(/~eyd~/g, eyd).replace(/~ey~/g, ey);
	path = path.replace(/~ex~/g, ex).replace(/~exm~/g, exm).replace(/~eym~/g, eym);
	path = path.replace(/~exd~/g, exd);

	$(id + ' svg path').attr('d', path);
//	$(id + ' svg circle').attr('cx', exd).attr('cy', drop);

}

function bottom_mid_wires(id, t1,l1,t2,l2)
{
	l1 -= 40; l2 += 40;
	var w= l2-l1+20, h= t1-t2+8;
	var sx= 18, ex= w-10, sy= 2, ey= h-4;
	var ds = h/3;

	$(id + ' svg').attr('width', w+'px').attr('height', h+'px').css({left: l1, top: t2});
	var path= 'M 40 ~sy~ C ~sxm~ ~sy~   ~sxm~ ~myb~   ~mx~ ~my~   ~exm~ ~myt~   ~exm~ 1   ~ex~ 1';

	path = path.replace(/~sx~/g, sx).replace(/~sy~/g, h);
	path = path.replace(/~sxm~/g, sx-20).replace(/~exm~/g, ex+20);
	path = path.replace(/~ex~/g, w-40);
	path = path.replace(/~mx~/g, w/2).replace(/~my~/g, h/2);
	path = path.replace(/~myb~/g, h/2+ds).replace(/~myt~/g, h/2-ds);

	$(id + ' svg path').attr('d', path);
}


function bottom_left_wires(id, t1,l1,t2,l2)
{
	var w= l2-l1-120, h= t1-t2+10;
	sx= 40; ex= w-40; sy= 2; ey= h-10; h2 = Math.floor(h * 2/ 3);
	var path= 'M ~sx~ ~ey~ C ~sxL~ ~ey~, ~sxL~ ~sy~ , ~exL~ ~sy~ L ~ex~ ~sy~';
		path = path.replace(/~sx~/g,  sx).replace(/~ey~/g, ey);
		path = path.replace(/~sxL~/g, sx - 40).replace(/~eyU~/g, ey-h2);
		path = path.replace(/~ex~/g,  ex).replace(/~sy~/g,  sy);
		path = path.replace(/~exL~/g, sx + (w/2));

	if ( w > 0 ){
		$(id + ' svg').attr('width', w+'px').attr('height', h+'px').css({left: l1- 40 , top: t2});
	}
	$(id + ' svg path').attr('d', path);
}

function top_left_wires(id, t1,l1,t2,l2)
{
	var w= l2-l1-140, h= t2-t1+10;
	sx= 40; ex= w; sy= 2; ey= h-10; h2 = Math.floor(h * 2 / 3);
	var path= 'M ~sx~ ~sy~ C ~sxL~ ~sy~, ~sxL~ ~ey~ , 60 ~ey~ L ~ex~ ~ey~';
		path = path.replace(/~sx~/g,  sx).replace(/~ey~/g, ey);
		path = path.replace(/~sxL~/g, sx-40).replace(/~eyU~/g, ey-h2);
		path = path.replace(/~ex~/g,  ex).replace(/~sy~/g,  sy);
	if ( w > 0 ){
		$(id + ' svg').attr('width', w+'px').attr('height', h+'px').css({left: l1- 40, top: t1});
	}
	$(id + ' svg path').attr('d', path);
}

function bottom_right_wires(id, t1,l1,t2,l2)
{
	var w= l1-l2+10, h= t1-t2+8;
	sx= 18; ex= w-10; sy= 2; ey= h-4; 
	var d= (ex-sx) / 2;

	$(id + ' svg').attr('width', w+'px').attr('height', h+'px').css({left: l2, top: t2});
	var path= 'M ~sx~ ~sy~ C ~sx10~ ~sy~ , ~ex10~ ~ey~ , ~ex~ ~ey~';
	path = path.replace(/~sx~/g,   sx).replace(/~sx10~/g, sx+d).replace(/~d~/g, d);
	path = path.replace(/~ex10~/g, ex-d).replace(/~ex~/g,   ex).replace(/~ex5~/g, ex-20);
	path = path.replace(/~sy~/g,   sy).replace(/~sy10~/g, sy+d);
	path = path.replace(/~ey10~/g, ey-d).replace(/~ey~/g,   ey);
	$(id + ' svg path').attr('d', path);
}

function top_right_wires(id, t1,l1,t2,l2)
{
	var w= l1-l2+10, h= t2-t1+10;
	sx= 18; ex= w-10; sy=  h-8; ey= 4;
	var d= (ex-sx) / 2;

	$(id + ' svg').attr('width', w+'px').attr('height', h+'px').css({left: l2, top: t1});
	var path= 'M ~sx~ ~sy~ C ~sx10~ ~sy~ , ~ex10~ ~ey~ , ~ex~ ~ey~';
	path = path.replace(/~sx~/g,   sx).replace(/~sx10~/g, sx+d).replace(/~d~/g, d);
	path = path.replace(/~ex10~/g, ex-d).replace(/~ex~/g,   ex).replace(/~ex5~/g, ex-20);
	path = path.replace(/~sy~/g,   sy).replace(/~sy10~/g, sy+d);
	path = path.replace(/~ey10~/g, ey-d).replace(/~ey~/g,   ey);
	$(id + ' svg path').attr('d', path);
}




function relationize(relation_id)
{
	var relation= $('[data-id="#"]'.replace('#', relation_id)).attr('data-name');
	return relation;
}

function nicename(relation_id, type)
{
	var relation= $('[data-id="#"]'.replace('#', relation_id)).attr('data-name');

	if ( typeof relation == 'undefined' ) return '';
	
	if ((relation.substr(-1) == 's') && (type == 'one-to-one'))
	{
		relation= relation.substr(0,relation.length-1);
	}
	relation = relation.toLowerCase().replace(/\b[a-z]/g, function(letter) {
    	return letter.toUpperCase();
	});

	relation = relation.replace(/\-/g, "&#8209;")

	return relation;
}

function modelreplace(id)
{
	var url = '/model/async/#/show'.replace('#', id);
	var model_id = id;
	var exists = $('#models [data-id="{id}"]'.replace('{id}',id)).length;

	var jqxhr = $.ajax( url )
		.done(function(data){
		
			if (!exists) 
			{
				model2html(data.model);
				drag_binding();
			}

			var header = getStub('update-header')
				.replace('[id]', model_id)
				.replace('[name]', data['model'].name);

			$('[data-id="#"] .header'.replace('#', model_id)).html(header);
			$('[data-id="#"] .body'.replace('#', model_id)).html('');

			var columns = data['columns'];
			for (var i = columns.length - 1; i >= 0; i--) {
				column2html(columns[i]);
			}

			connectboxes();
			wired();
			$('#editModal').trigger('modal:hide', $('#editModal')).html('');
		});
}

function modelupdate(id, action, var1, var2)
{
	var jqxhr = $.ajax( "/model/async/"+id+"/"+action+"/"+var1+"/"+var2 )
		.done(function(data){});
}

function combi2html(data)
{

	var models = data['models'];
	for (var i = models.length - 1; i >= 0; i--) {
		model2html(models[i]);
	}
	var columns = data['columns'];
	for (var i = columns.length - 1; i >= 0; i--) {
		column2html(columns[i]);
	}

}

function column2html(column){

//	console.log( column );

	var field = getStub('html-field');

	var dataconnect = '',
		relation = column['type'];
	
	if (column['group'] == 'relation'){
		var relation_id = column['relation_id'];
		if ( relation_id ){

			if (column['type'] != 'parent-id')
			{
				dataconnect= 'data-connect="'+relationize(relation_id)+'"';
			}
			relation= nicename(relation_id, column['type']);
		}
	}
	
	if (column['title'].length) relation = column['title'];
	var is_narrow = '';
	if ( relation.length > 14 ) is_narrow = 'narrow';

	field = field.replace(/\[typecol\]/g, column['type']);
	field = field.replace(/\[connect\]/g, dataconnect);
	field = field.replace(/\[reltype\]/g, relation);
	field = field.replace(/\[name\]/g,    column['name']);
	field = field.replace(/\[narrow\]/g,  is_narrow);

	var selector = "[data-id='#'] .body".replace('#', column['model_id'] );
	
	$(selector).prepend(field);

}

function model2html(model){
	
	var modelname = model.name.toLowerCase().replace('(s)', 's');

	var html = getStub('html-model');

	html = html.replace(/\[zidx\]/g,   model.id );
	html = html.replace(/\[type\]/g,   model.type );
	html = html.replace(/\[mname\]/g,  modelname );
	html = html.replace(/\[id\]/g,     model.id );
	html = html.replace(/\[x\]/g,      model.screen_left );
	html = html.replace(/\[y\]/g,      model.screen_top );
	html = html.replace(/\[name\]/g,   model.name );

	console.log( model );

	$( "#models" ).append( html );
//	console.log( model );
}

function connectboxes()
{
//	var stub='<div id="[id]"><div class="box-in"></div><div class="box-out"></div></div>';	
	var stub=''
	stub += '<div id="[id]">';	
	stub += '<svg width="190px" height="160px" version="1.1" xmlns="http://www.w3.org/2000/svg">';
  	stub += '<path d="M10 80 Q 52.5 10, 95 80 T 180 80" stroke="#222" stroke-width=".5" fill="transparent"/>';	
//  	stub += '<circle cx="5" cy="5" r="4" stroke="black" stroke-width="" fill="red"></circle>';
	stub += '</svg>';
	stub += '</div>';

	$("#wires").html('');
	$('[data-connect]').each(function(idx){ 

		var mynameis  = $(this).attr('data-connect');
		var modelname = '[data-name="#"]'.replace('#',mynameis);
		if ($(modelname).length > 0 )
		{
//			var id=  $(this).parent().parent().attr('data-name') + '-' + mynameis;
			var id = 'wire-' + idx;

			if ($('#'+id).length == 0 )
			{
				$("#wires").append(stub.replace('[id]', id));
				$(this).attr('data-wire', id)
			}
		}
	});
}


var pointerX;
var pointerY;
var zoom = 1;

$('#wires, #models').css({zoom: zoom});

function drag_binding(gridsize)
{
	var zoomgridsize = gridsize / zoom;

	$( '.model' ).draggable({
		start: function( event, ui ) {
			pointerY = (event.pageY - $('#models').offset().top) / zoom - parseInt($(event.target).css('top'));
			pointerX = (event.pageX - $('#models').offset().left) / zoom - parseInt($(event.target).css('left'));
		},
		drag: function( event, ui ) {
			var canvasTop= 0;
			var canvasLeft= 0;
			
			if (gridsize){
				event.pageY = Math.round( event.pageY / zoomgridsize ) * zoomgridsize;
				event.pageX = Math.round( event.pageX / zoomgridsize ) * zoomgridsize;
				pointerY =  Math.round( pointerY / zoomgridsize ) * zoomgridsize;
				pointerX =  Math.round( pointerX / zoomgridsize ) * zoomgridsize;
					
			}

			ui.position.top  = Math.round((event.pageY - canvasTop) / zoom - pointerY); 
			ui.position.left = Math.round((event.pageX - canvasLeft) / zoom - pointerX); 

			ui.offset.top = Math.round(ui.position.top + canvasTop);
			ui.offset.left = Math.round(ui.position.left + canvasLeft);

			wired();
		},
		stop: function( event, ui ) {
			var id= $(ui.helper[0]).attr('data-id');
			var rndL = Math.floor(ui.position.left * zoom);
			var rndT = Math.floor(ui.position.top * zoom);
			modelupdate( id, 'position', rndL, rndT );
			wired();
		}
	});
}


$(function() {

	var project_id= $('#models').attr('data-project-id');

// Edit window

	if ( project_id > 0 )
	{
		var jqxhr = $.ajax( "/project/async/" + project_id)
			.done(function(data) {

			combi2html(data);
			connectboxes();
			wired();

			drag_binding( null );
			
			$( '.model' ).on('mousedown', function(event) { 
				$('.model').css('z-index','1');
				$( this ).css('z-index','1000');
			});
		});
	}

// Modals

	$(document).on('modal:show', function(e,d){
		$(d).addClass('open');
		$(document).find('.reveal-modal-bg').addClass('open');
	})
	$(document).on('modal:hide', function(e,d){
		$(document).find('.reveal-modal-bg').removeClass('open');
		$(d).removeClass('open');
	})

	$(document).on('click', '[data-close-modal]', function(){ 
		$(document).trigger('modal:hide', $(this).closest('[data-reveal]'));
	})

	$(document).on('click', '[data-sure]', function(e){
		if ( $(this).next().hasClass('open') )
			$(this).next().removeClass('open');
		else
			$(this).next().addClass('open');
	});

	$(document).on('click', '[data-not-sure]', function(e){
		$(this).closest('.sure.open').removeClass('open');
	});


// Legenda
	
	$('[data-action="zoom-in"]').click( function(e){
		zoom -= 0.1;
		e.preventDefault();
		$('#wires, #models').css({zoom: zoom});
	});
	$('[data-action="zoom-out"]').click( function(e){
		zoom += 0.1;
		e.preventDefault();
		$('#wires, #models').css({zoom: zoom});
	});


	$('[data-action="grid"]').click( function(e){
		if ( $(this).hasClass('active')){
			$(this).removeClass('active');
			drag_binding( null );
		}else{
			$(this).addClass('active');
			drag_binding( 20 );
			// and put all on grid...
		}
	});


// Modals

	$(document).on('click', '[data-close-modal]', function(e){
		$(this).parent().removeClass('open');
	});

// Any row in a Modal

	$(document).on('click', '.delete-box', function(e){
		$(this).closest('li').remove();
	});

});

$(function() {

	
	var project_id= $('#models').attr('data-project-id');

// Sidebar
//	$('[data-action="project"]').click( function(e){
//		$(document).trigger('modal:show', $('#projectModal'));
//	});
// Sidebar
	$('[data-action="project"]').click( function(e){
	
		$('#projectModal').html(getStub('modal-project')).trigger('modal:show', '#projectModal');
		$.ajax("/project/async/{id}/edit".replace('{id}', $('[data-project-id]').attr('data-project-id') ))
			.done(function(response){ 
				$('#projectModal').html(response); 
			});

	})

	$('[data-upload]').click( function(e){
		
		var upload = $(this).attr('data-upload');
		var fileSelect = document.getElementById('file-select');
		var files = fileSelect.files;
		var formData = new FormData();
		for (var i = 0; i < files.length; i++) {
  			var file = files[i];
  			formData.append('files[]', file, file.name);
		}
		formData.append('uploadtype', upload);
		formData.append('project_id', $('#models').attr('data-project-id') );
		
		var url = '/project/async/import';
		var xhr = new XMLHttpRequest();
		xhr.open('POST', url, true);
		xhr.onload = function () {
			if (xhr.status === 200) {
				var response = JSON.parse(this.responseText);
				window.location = '/project/' + response;
			} 
		};
		xhr.send(formData);

	})

// Upload
// Sidebar
	$('#file-select').change(function(){
		var fileSelect = document.getElementById('file-select');
		var files = fileSelect.files;
		var formData = new FormData();
		for (var i = 0; i < files.length; i++) {
  			var file = files[i];
  			formData.append('files[]', file, file.name);
		}
		$(document).trigger('modal:show', $('#uploadModal'));

	});

	// make li clickable
	$('.projects li:not(.new)').click( function(e){
		var $li= $(this);
		setTimeout( function(){
			var locked = $li.hasClass('locked');
			if ( !locked )
			{
				var href= $li.find('a').attr('href');
				window.location.href= href;
			}
		}, 10);
	});


// Modals

	$(document).on('click', '[data-save-project]', function(e){
		var name = $('[name="projectname"]').val();
		var post = "/project/async/rename/" + project_id + '/' + encodeURIComponent(name);
		var url  = "/project/async/{id}/update".replace('{id}', project_id);
		var form = $('#projectModal').find('form');
		
		$.ajax( { url:url, type: "POST", data: form.serialize() })
			.done(function(response){ 
					
				$.ajax(post).done(	function(response){ 
					name = response.name;
					document.title = name;
					$('[name="projectname"]').val(name);
					$('[data-project-name]').attr('data-project-name', name);
					$(document).trigger('modal:hide', $('#projectModal'));
				});

			});


	})

// New project
	$('.new').click(function(){
		$('.new .namerow').addClass('open');
	});

	$('.new .button').click(function(){
		$('.valifail').removeClass('show');

		var name= $('.new').find("input[type='text']").val();
		if (name==''){
			$('.valifail').addClass('show');
		}else{
			window.location.href= '/project/make/' + name;
		}
	});
	

});

$(function() {

	var project_id= $('#models').attr('data-project-id');

// Sidebar
	$('[data-action="add-config"]').click( function(e){

		$('#editModal').html(getStub('modal-config')).trigger('modal:show', '#editModal');
		$.ajax("/model/async/{id}/config".replace('{id}', $('[data-project-id]').attr('data-project-id') ))
			.done(function(response){ 
				$('#editModal').html(response); 
			});
	});


	$(document).on('click', '[data-save-config]', function(e){
		
		var form     = $('#editModal').find('form');
		var model_id = form.attr('data-model-id');
		if ( model_id == '' ) model_id = 0;
		var url      = "/model/async/{id}/update".replace('{id}', model_id);

		$('#editModal ul').hide();
		$('#editModal .header input').hide();
		$('#editModal .header').append('Saving...');
		
		$.ajax( { url:url, type: "POST", data: form.serialize() })
			.done(function(response){ 
					
				modelreplace(response.id); 

			});
	
	});

});

$(function() {


	var project_id= $('#models').attr('data-project-id');

// Sidebar
	$('[data-action="add-module"]').click( function(e){

		$('#editModal').html(getStub('modal-module')).trigger('modal:show', '#editModal');
		$.ajax("/module/async/{id}/edit".replace('{id}', $('[data-project-id]').attr('data-project-id') ))
			.done(function(response){ 
				$('#editModal').html(response); 
			});
	})


// Modals

	$(document).on('click', '[data-delete]', function(e){
		var $li= $(this).closest('li');
		if ( $(this).next().hasClass('open') ){
			setTimeout( function(){ $li.addClass('locked'); }, 50 );
			$(this).next().removeClass('open');
		}else{
			$li.addClass('locked');
			$(this).next().addClass('open');
		}
	});
	
	$(document).on('click', '[data-close-sure]', function(e){
		var $li= $(this).closest('.sure');
			$li.removeClass('open');
	});


	$(document).on('click', '[data-save-module]', function(e){
		
		var form     = $('#editModal').find('form');
		var url      = "/module/async/{id}/update".replace('{id}', project_id);

		$('[name*="module_label"]').each( function(){
			if ($(this).val()=='') $(this).val('Noname'); 
		});

		$('#editModal ul').hide();
		$('#editModal .header input').hide();
		$('#editModal .header').append('Saving...');
		
		$.ajax( { url:url, type: "POST", data: form.serialize() })
			.done(function(response){ 
					
				$(document).trigger('modal:hide', $('#editModal'));				

			});
	
	});


});

$(function() {

	var project_id= $('#models').attr('data-project-id');

// Sidebar
	$('[data-action="add-model"]').click( function(e){
	
		$('#editModal').html(getStub('modal-model')).trigger('modal:show', '#editModal');
		$.ajax("/model/async/{id}/create".replace('{id}', $('[data-project-id]').attr('data-project-id') ))
			.done(function(response){ 
				$('#editModal').html(response); 
			});

	})

// Modals

	$(document).on('click', '[data-model-edit]', function(e){
		e.preventDefault();
		$('#editModal').html(getStub('modal-model')).trigger('modal:show', '#editModal');
		$.ajax("/model/async/{id}/edit/".replace('{id}', $(this).attr('data-model-edit') ))
			.done(function(response){ $('#editModal').html(response); });
	});

	$(document).on('change', '[name="column_group[]"]', function(e){
		var $li = $(this).parent().parent();
		$li.attr('class', $(this).val());
	});

	$(document).on('click', '[data-model-delete]', function(e){
		var id = $(this).attr('data-model-delete');
		var url= "/model/async/{id}/delete".replace('{id}', id);

		$.ajax(url).done(function(response){ 
			$('[data-id="#"]'.replace('#',id)).remove();
			$('#wires').html('');
			connectboxes();
			wired();

			$('#editModal').trigger('modal:hide', '#editModal').html('');
		});

		

	});

	$(document).on('click', '[data-add-modelrow]', function(e){
		$('#editModal .columns').append($('.blueprint').html());
		$('#editModal .columns').sortable();
		$('#editModal ul').disableSelection();
	});

	$(document).on('click', '[data-save-model]', function(e){
		
		var form     = $('#editModal').find('form');
		var model_id = form.attr('data-model-id');
		if ( model_id == '' ) model_id = 0;
		var url      = "/model/async/{id}/update".replace('{id}', model_id);

		$('#editModal ul').hide();
		$('#editModal .header input').hide();
		$('#editModal .header').append('Saving...');
		
		$.ajax( { url:url, type: "POST", data: form.serialize() })
			.done(function(response){ 
					
				modelreplace(response.id); 

			});
	
	});


});
//# sourceMappingURL=brickle.js.map
